# MagSport

Project link on [GitLab](https://gitlab.com/jean-baptiste.huyghe/magsport).
## Information
This project is software for manage stocks and products in different stores.

It's a tutored project for Polytech Tours in 2019-2020.

## Contributors
- BARTHOLIN Florian
- HUYGHE Jean-Baptiste
- RUI Xu
- WANG Kuo

## How to launch this application?
1. Open the project with Eclipse or Intellij IDEA (two Java IDE)
2. Compile and start the [Main](./MagSport/src/magsport/Main.java) class/file.

## How to connect to this app?
With the default login and password: `ceo` `password`.

For all others logins of the dataset, they are available in the file [CompanyExample.java](./MagSport/src/test/magsport/CompanyExample.java).