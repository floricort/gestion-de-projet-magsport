package test.magsport.controller;

import magsport.controller.AiselAddEditController;
import magsport.controller.MagSportController;
import magsport.model.Company;
import magsport.model.Department;
import magsport.model.Store;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import test.magsport.CompanyExample;
import static org.junit.Assert.*;

import java.security.NoSuchAlgorithmException;

import static org.junit.Assert.assertEquals;

public class AiselAddEditControllerTest {
     MagSportController magSportController;
    @Before
    public void before() throws Exception {
    }

    @After
    public void after() throws Exception {
    }

    @Test
    public void testAddNewDepartment() throws NoSuchAlgorithmException {

        CompanyExample companyExample = new CompanyExample();
        Company company = companyExample.getCompanyExample();
        Store store = company.getStores().get(0);
        magsport.controller.AiselAddEditController aiselAddEditController = new magsport.controller.AiselAddEditController(magSportController);
        aiselAddEditController.addNewDepartment("newde",store);
        Department department = aiselAddEditController.findDepartment("newde",store);
        assertEquals("newde",department.getName());

    }

    @Test
    public void testDeleteDepartment() throws NoSuchAlgorithmException {
        CompanyExample companyExample = new CompanyExample();
        Company company = companyExample.getCompanyExample();
        Store store = company.getStores().get(0);
        magsport.controller.AiselAddEditController aiselAddEditController = new magsport.controller.AiselAddEditController(magSportController);
        aiselAddEditController.addNewDepartment("newde",store);
        Department department = aiselAddEditController.findDepartment("newde",store);
        aiselAddEditController.deleteDepartment(department,store);
        department = aiselAddEditController.findDepartment("newde",store);
        assertEquals(null,department);
    }
    @Test
    public void testFindDepartment() throws NoSuchAlgorithmException {
        CompanyExample companyExample = new CompanyExample();
        Company company = companyExample.getCompanyExample();
        Store store = company.getStores().get(0);
        magsport.controller.AiselAddEditController aiselAddEditController = new magsport.controller.AiselAddEditController(magSportController);
        Department department = aiselAddEditController.findDepartment("newde",store);
        assertEquals(null,department);
        department = aiselAddEditController.findDepartment("Badminton",store);
        assertEquals("Badminton",department.getName());
    }
}
