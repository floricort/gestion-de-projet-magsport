package test.magsport.controller;

import magsport.controller.EmployeesController;
import magsport.controller.MagSportController;
import magsport.model.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
public class EmployeesControllerTest {
    MagSportController magSportController = new MagSportController();

    public EmployeesControllerTest() throws IOException, NoSuchAlgorithmException {
    }

    @Before
    public void before() throws Exception {
    }

    @After
    public void after() throws Exception {
    }

    @Test
    public void testGetEmployeesList() throws NoSuchAlgorithmException {
        User user = new User("CEO first name","CEO last name","ceo","password", Role.ROLE_CEO);
        magSportController.setLoggedInUser(user);
        EmployeesController employeesController = new EmployeesController(magSportController);
        ArrayList<User> users = employeesController.getEmployeesList();
        assertEquals("dylan.motard",users.get(0).getLogin());
    }
}
