package test.magsport.controller;
import magsport.controller.AiselAddEditController;
import magsport.controller.EmployeeController;
import magsport.controller.MagSportController;
import magsport.model.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import test.magsport.CompanyExample;
import static org.junit.Assert.*;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
public class EmployeeControllerTest {
    MagSportController magSportController = new MagSportController();

    public EmployeeControllerTest() throws IOException, NoSuchAlgorithmException {
    }

    @Before
    public void before() throws Exception {
    }

    @After
    public void after() throws Exception {
    }

    @Test
    public void testGetStores() throws NoSuchAlgorithmException {
        User user = new User("CEO first name","CEO last name","ceo","password", Role.ROLE_CEO);
        CompanyExample companyExample = new CompanyExample();
        Company company = companyExample.getCompanyExample();
        ArrayList<Store> stores1 = company.getStores();
        magSportController.setLoggedInUser(user);
        EmployeeController employeeController = new EmployeeController(magSportController);
        ArrayList<Store> stores2 = employeeController.getStores();
        assertEquals(stores1,stores2);
    }
    @Test
    public void testCreateUser() throws NoSuchAlgorithmException {
        CompanyExample companyExample = new CompanyExample();
        Company company = companyExample.getCompanyExample();
        Store store = company.getStores().get(0);
        EmployeeController employeeController = new EmployeeController(magSportController);
        employeeController.createUser("abc","edf","abcedf",store,Role.ROLE_STORE_MANAGER);
        User user = store.getUser("abcedf");
        assertEquals("abcedf",user.getLogin());
    }
}
