package test.magsport.controller; 

import org.junit.Test;

import magsport.controller.MagSportController;

import org.junit.Before;

import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import org.junit.After; 


public class MagSportControllerTest { 

	/**
	 * delete a save if exist
	 */
	private void deleteSave() {
		File save = new File(MagSportController.COMPANY_FILE);
		if (save.exists()) {
			save.delete();
		}
	}

	
	/** 
	 * 
	 * Method: saveCompany() 
	 * 
	 */
	@Test
	public void testSaveCompanyExport() {
		try {
			deleteSave();
			MagSportController magSportController = new MagSportController();
			magSportController.saveCompany();
			assert(new File(MagSportController.COMPANY_FILE).exists());
			deleteSave();
		} catch (SecurityException | NoSuchAlgorithmException | IOException e) {
		}
	}


	/** 
	 * 
	 * Method: setLoggedInUser(User user) 
	 * 
	 */ 
	@Test
	public void testSetLoggedInUser() throws Exception { 
	} 

	/** 
	 * 
	 * Method: getLoggedInUser() 
	 * 
	 */ 
	@Test
	public void testGetLoggedInUser() throws Exception { 
		//TODO: Test goes here... 
	} 

	/** 
	 * 
	 * Method: getLogger() 
	 * 
	 */ 
	@Test
	public void testGetLogger() throws Exception { 
		//TODO: Test goes here... 
	} 


	/** 
	 * 
	 * Method: startApplication() 
	 * 
	 */ 
	@Test
	public void testStartApplication() { 
			MagSportController magSportController;
			try {
				magSportController = new MagSportController();
				assert(magSportController.getCompany() != null);	
			} catch (SecurityException e) {				
			} catch (NoSuchAlgorithmException e) {
			} catch (IOException e) {
			}
	} 

	/** 
	 * 
	 * Method: loadCompany() 
	 * 
	 */ 
	@Test
	public void testLoadCompany() {
		try {
			// create company.ser
			MagSportController magSportController1 = new MagSportController();
			magSportController1.saveCompany();
			// load company.ser
			MagSportController magSportController2 = new MagSportController();
			assert(magSportController1.getCompany().equals(magSportController2.getCompany()));
		} catch (SecurityException | NoSuchAlgorithmException | IOException e) { }
		
	}


} 
