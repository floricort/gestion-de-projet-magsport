package test.magsport.controller;

import magsport.controller.MagSportController;
import magsport.controller.ProductAddEditController;
import magsport.model.Company;
import magsport.model.Department;
import magsport.model.Product;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import test.magsport.CompanyExample;
import org.junit.Assert.*;

import static org.junit.Assert.assertTrue;

/**
 * ProductAddEditController Tester.
 *
 * @author <Authors name>
 * @since <pre>Feb 1, 2020</pre>
 * @version 1.0
 */
public class ProductAddEditControllerTest {
    Company company;
    ProductAddEditController productAddEditController;
    MagSportController magSportController;
    @Before
    public void before() throws Exception {
        magSportController = new MagSportController();
        productAddEditController = new ProductAddEditController(magSportController);
        CompanyExample companyExample = new CompanyExample();
        company = companyExample.getCompanyExample();
    }

    @After
    public void after() throws Exception {
    }

    /**
     *
     * Method: ProductAdd(String name, String code, String description, String stock, Department department)
     *
     */
    @Test
    public void testProductAdd() throws Exception {
        Product product = new Product(12345678,"test1","test1");
        product.setStock(20);
        Department department = company.getStores().get(0).getDepartments().get(0);
        productAddEditController.ProductAdd("test1", "12345678", "test1","20",department);
        assertTrue(department.getProducts().size() == 4);
        System.out.println(department.getProducts().get(0).getStock());
    }


}
