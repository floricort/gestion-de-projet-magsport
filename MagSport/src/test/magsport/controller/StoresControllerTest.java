package test.magsport.controller;

import magsport.controller.MagSportController;
import magsport.controller.StoresController;
import magsport.model.Company;
import magsport.model.Store;
import magsport.model.User;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import test.magsport.CompanyExample;

import java.util.ArrayList;

import org.junit.Assert.*;

import static org.junit.Assert.assertEquals;

/**
 * StoresController Tester.
 *
 * @author <WANGkuo>
 * @since <pre>Jau 20, 2020</pre>
 * @version 1.0
 */
public class StoresControllerTest {
    Company company;
    MagSportController magSportController;
    StoresController storesController;
    @Before
    public void before() throws Exception {
        CompanyExample companyExample = new CompanyExample();
        company = companyExample.getCompanyExample();
        magSportController = new MagSportController();
        storesController = new StoresController(magSportController);
    }

    @After
    public void after() throws Exception {
    }


    /**
     *
     * Method: getStore(String name)
     *
     */
    @Test
    public void testGetStore() throws Exception {
        User user = company.getCeo();
        Store store = storesController.getStore("Decathlon North");
        assertEquals("Decathlon North",store.getName());
    }


}

