package test.magsport.controller;

import magsport.controller.MagSportController;
import magsport.controller.StoreController;
import magsport.controller.StoresController;
import magsport.model.Company;
import magsport.model.Department;
import magsport.model.Store;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import test.magsport.CompanyExample;

import org.junit.Assert.*;

import static org.junit.Assert.assertEquals;

/**
 * StoreController Tester.
 *
 * @author <Authors name>
 * @since <pre>Feb 1, 2020</pre>
 * @version 1.0
 */
public class StoreControllerTest {
    MagSportController magSportController;
    StoreController storeController;
    Company company;
    CompanyExample companyExample;
    @Before
    public void before() throws Exception {
        magSportController = new MagSportController();
        storeController = new StoreController(magSportController);
        companyExample = new CompanyExample();
        company = companyExample.getCompanyExample();
    }

    @After
    public void after() throws Exception {
    }

    /**
     *
     * Method: getAisel(String name, Store currentStore)
     *
     */
    @Test
    public void testGetAisel() throws Exception {
        Store store = company.getStores().get(0);
        //Department department = store.getDepartments().get(0);
        Department department1 = storeController.getAisel("Basketball",store);
        Department department2 = storeController.getAisel("Badminton", store);
        Department department3 = storeController.getAisel("Archery", store);
        assertEquals("Basketball", department1.getName());
        assertEquals("Badminton", department2.getName());
        assertEquals("Archery", department3.getName());
    }

}
