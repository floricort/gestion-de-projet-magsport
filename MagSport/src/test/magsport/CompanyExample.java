package test.magsport;

import java.security.NoSuchAlgorithmException;

import magsport.model.Company;
import magsport.model.Department;
import magsport.model.Product;
import magsport.model.Role;
import magsport.model.Store;
import magsport.model.User;



public class CompanyExample {
	
	
	/**
	 * Return a example company
	 * @return 
	 * @throws NoSuchAlgorithmException 
	 */
	public Company getCompanyExample() throws NoSuchAlgorithmException {
		Company company = new Company();
		
		// create stores
		Store decathlonNorth = new Store("Decathlon North", "Tours", "28 Edward Street");
		Store decathlonSouth = new Store("Decathlon South", "Tours", "2 Nationnl Avenue");
		Store decathlonWest = new Store("Decathlon West", "Tours", "10 Boulevard of Dreams");
		Store decathlonEast = new Store("Decathlon East", "Orleans", "19 Church Place");
		// link stores
		company.addStore(decathlonNorth);
		company.addStore(decathlonSouth);
		company.addStore(decathlonWest);
		company.addStore(decathlonEast);
		
		// create departments
		Department decathlonNorthBadminton = new Department("Badminton");
		Department decathlonNorthArchery = new Department("Archery");
		Department decathlonNorthBasketball = new Department("Basketball");
		Department decathlonSouthBasketball = new Department("Basketball");
		Department decathlonSouthFootball = new Department("Football");
		// link departments
		decathlonNorth.addDepartment(decathlonNorthBadminton);
		decathlonNorth.addDepartment(decathlonNorthArchery);
		decathlonNorth.addDepartment(decathlonNorthBasketball);
		decathlonSouth.addDepartment(decathlonSouthBasketball);
		decathlonSouth.addDepartment(decathlonSouthFootball);
		
		// create products
		Product decathlonNorthBadmintonYellowRacket = new Product(1892553, "Yellow Long Racket", "Racket for badminton, yellow color, adult size, junior level");
		Product decathlonNorthBadmintonGreenRacket = new Product(1892555, "Green Medium Racket", "Racket for badminton, green color, junior size, junior level");
		Product decathlonNorthBadmintonRedRacket = new Product(1892246, "Red Long Racket", "Racket for badminton, red color, adult size, profesional level");
		Product decathlonNorthArcheryCompoundBow = new Product(1892553, "Compound bow", "Compound bow, carbon material, branches 26 pounds");
		Product decathlonNorthArcheryBow = new Product(6298, "Bow", "Bbow, aluminum material, branches 22 pounds");
		Product decathlonNorthArcheryArrows = new Product(9834272, "10 arrows", "10 arrows 63 inchs, carbon tube");
		Product decathlonSouthBasketBall = new Product(32458, "Basket Ball", "Professional ball, 14.5 inchs");
		// link products
		decathlonNorthBadminton.addProduct(decathlonNorthBadmintonYellowRacket);
		decathlonNorthBadminton.addProduct(decathlonNorthBadmintonGreenRacket);
		decathlonNorthBadminton.addProduct(decathlonNorthBadmintonRedRacket);
		decathlonNorthArchery.addProduct(decathlonNorthArcheryCompoundBow);
		decathlonNorthArchery.addProduct(decathlonNorthArcheryBow);
		decathlonNorthArchery.addProduct(decathlonNorthArcheryArrows);
		decathlonSouthBasketball.addProduct(decathlonSouthBasketBall);
		
		// create users
		User decathlonNorthSMDylan = new User("Dylan", "MOTARD", "dylan.motard", "dylan", Role.ROLE_STORE_MANAGER);
		User decathlonSouthSMMonique = new User("Monique", "LELOUP", "monique.leloup", "monique", Role.ROLE_STORE_MANAGER);
		User decathlonWestSMBernadette = new User("Bernadette", "FIFOU", "bernadette.fifou", "bernadette", Role.ROLE_STORE_MANAGER);
		User decathlonNorthBadmintonLouis = new User("Louis", "DRUCKER", "louis.drucker", "louis", Role.ROLE_DEPARTMENT_MANAGER);
		User decathlonNorthArcheryEnzo = new User("Enzo", "GERNAIS", "enzo.gernais", "enzo", Role.ROLE_DEPARTMENT_MANAGER);
		User decathlonNorthBasketballLea = new User("Lea", "ROSE", "lea.rose", "lea", Role.ROLE_DEPARTMENT_MANAGER);
		User decathlonSouthBasketballMurielle = new User("Murielle", "Chamut", "murielle.chamut", "murielle", Role.ROLE_DEPARTMENT_MANAGER);
		User decathlonNorthOdile = new User("Odile", "FOULE", "odile.foule", "odile", Role.ROLE_STORE_MANAGER);
		User decathlonNorthClement = new User("Clement", "GUICHARD", "clement.guichard", "clement", Role.ROLE_STORE_MANAGER);
		User decathlonSouthLaura = new User("Laura", "DUFOUR", "laura.dufour", "laura", Role.ROLE_STORE_MANAGER);
		User decathlonSouthFootballPeter = new User("peter", "mark", "peter.mark", "peter", Role.ROLE_DEPARTMENT_MANAGER);
		// link users
		decathlonNorth.addUser(decathlonNorthSMDylan);
		decathlonSouth.addUser(decathlonSouthSMMonique);
		decathlonWest.addUser(decathlonWestSMBernadette);
		decathlonNorth.addUser(decathlonNorthBadmintonLouis);
		decathlonNorth.addUser(decathlonNorthArcheryEnzo);
		decathlonNorth.addUser(decathlonNorthBasketballLea);
		decathlonSouth.addUser(decathlonSouthBasketballMurielle);
		decathlonNorth.addUser(decathlonNorthOdile);
		decathlonNorth.addUser(decathlonNorthClement);
		decathlonSouth.addUser(decathlonSouthLaura);
		// link person in charge of ...
		decathlonNorth.setPersonInCharge(decathlonNorthSMDylan);
		decathlonSouth.setPersonInCharge(decathlonSouthSMMonique);
		decathlonWest.setPersonInCharge(decathlonWestSMBernadette);
		

		decathlonNorthBadminton.setPersonInCharge(decathlonNorthBadmintonLouis);
		decathlonNorthArchery.setPersonInCharge(decathlonNorthArcheryEnzo);
		decathlonNorthBasketball.setPersonInCharge(decathlonNorthBasketballLea);
		decathlonSouthBasketball.setPersonInCharge(decathlonSouthBasketballMurielle);	

		decathlonNorth.setPersonInCharge(decathlonNorthBadmintonLouis);
		decathlonNorth.setPersonInCharge(decathlonNorthArcheryEnzo);
		decathlonNorth.setPersonInCharge(decathlonNorthBasketballLea);
		decathlonSouth.setPersonInCharge(decathlonSouthBasketballMurielle);

		//link person in charge of departement...
		decathlonNorthBadminton.setPersonInCharge(decathlonNorthBadmintonLouis);
		decathlonNorthArchery.setPersonInCharge(decathlonNorthArcheryEnzo);
		decathlonNorthBasketball.setPersonInCharge(decathlonNorthBasketballLea);
		decathlonSouthBasketball.setPersonInCharge(decathlonSouthBasketballMurielle);
		decathlonSouthFootball.setPersonInCharge(decathlonSouthFootballPeter);
		
		return company;
	}
}
