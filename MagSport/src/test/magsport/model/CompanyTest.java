package test.magsport.model; 

import magsport.model.*;
import org.junit.Test;
import org.junit.Before; 
import org.junit.After;
import test.magsport.CompanyExample;

import java.lang.reflect.Field;
import java.util.ArrayList;

import static org.junit.Assert.*;

/**
* Company Tester. 
* 
* @author <Authors name> 
* @since <pre>Jan 28, 2020</pre> 
* @version 1.0 
*/ 
public class CompanyTest { 

@Before
public void before() throws Exception { 
} 

@After
public void after() throws Exception { 
} 

/** 
* 
* Method: getCeo() 
* 
*/ 
@Test
public void testGetCeo() throws Exception {
    //given
    final Company company = new Company();
    final Field field = company.getClass().getDeclaredField("ceo");
    field.setAccessible(true);
    User user = new User("peter","smith", "login", "password", Role.ROLE_CEO);
    field.set(company, user);

    //when
    final User result = company.getCeo();

    //then
    assertEquals("field wasn't retrieved properly", result, user);
} 

/** 
* 
* Method: getStores() 
* 
*/ 
@Test
public void testGetStores() throws Exception {
    final Company company = new Company();
    final Field field = company.getClass().getDeclaredField("stores");
    field.setAccessible(true);
    ArrayList<Store> test1 = new ArrayList<>();
    for(int i = 0; i < 4; i++){
        test1.add(new Store("nike", "beijing","changan Road"));
    }
    field.set(company, test1);

    final ArrayList<Store> result = company.getStores();

    assertArrayEquals("field wasn't retrieved properly", test1.toArray(),result.toArray());
} 

/** 
* 
* Method: setCeo(User ceo) 
* 
*/ 
@Test
public void testSetCeo() throws Exception {
    final Company company = new Company();
    User user = new User("peter","smith", "login", "password", Role.ROLE_CEO);
    company.setCeo(user);
    final Field field = company.getClass().getDeclaredField("ceo");
    field.setAccessible(true);
    assertEquals("Fields didn't match", field.get(company), user);
} 

/** 
* 
* Method: setStores(ArrayList<Store> stores) 
* 
*/ 
@Test
public void testSetStores() throws Exception {
    ArrayList<Store> test1 = new ArrayList<>();
    for(int i = 0; i < 4; i++){
        test1.add(new Store("nike"+Integer.toString(i), "beijing","changan Road"));
    }
    final Company company = new Company();
    company.setStores(test1);
    final Field field = company.getClass().getDeclaredField("stores");
    field.setAccessible(true);
    assertEquals("Fields didn't match", field.get(company), test1);
} 


/** 
* 
* Method: getUser(String login) 
* 
*/ 
@Test
public void testGetUser() throws Exception {
    String login1 = "dylan.motard";
    String login2 = "louis.drucker";
    CompanyExample companyExample = new CompanyExample();
    Company company = companyExample.getCompanyExample();
    assertEquals(company.getUser("ceo").getRole(), Role.ROLE_CEO);
    assertEquals(company.getUser(login1).getRole(), Role.ROLE_STORE_MANAGER);
    assertEquals(company.getUser(login2).getRole(),Role.ROLE_DEPARTMENT_MANAGER);
} 


/** 
* 
* Method: toString() 
* 
*/ 
@Test
public void testToString() throws Exception { 
    
    Company company = new Company();
    ArrayList<Store> stores = new ArrayList<>();
    for(int i = 0; i < 4; i++){
        stores.add(new Store("store"+Integer.toString(i),
                "city"+Integer.toString(i), "address"+Integer.toString(i)));
    }
    company.setStores(stores);
    
    String text1 = "<Company:\n" +
    		"\t" + company.getCeo().toString() + "\n" +
            "\t" + company .getStores().toString() + "\n" +
            ">\n";
    assertEquals(text1, company.toString());
} 


/** 
* 
* Method: equals(Object obj) 
* 
*/ 
@Test
public void testEquals() throws Exception { 
    CompanyExample companyExample = new CompanyExample();
    User user1 = new User("peter","smith","loginuser1", "passworduser1", Role.ROLE_CEO);
    ArrayList<Store> stores = new ArrayList<>();
    for(int i = 0; i < 4; i++){
        stores.add(new Store("store"+Integer.toString(i),
                "city"+Integer.toString(i), "address"+Integer.toString(i)));
    }
    Company company1 = new CompanyExample().getCompanyExample();
    Company company2 = new CompanyExample().getCompanyExample();
    Company company3 = new CompanyExample().getCompanyExample();
    Company company4 = new CompanyExample().getCompanyExample();
    company3.setCeo(user1);
    company4.setStores(stores);
    assertTrue(company1.equals(company2));
    assertFalse(company1.equals(company3));
    assertFalse(company1.equals(company4));

} 


} 
