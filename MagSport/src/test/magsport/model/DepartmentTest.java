package test.magsport.model; 

import magsport.model.*;
import org.junit.Test;
import org.junit.Before; 
import org.junit.After;
import test.magsport.CompanyExample;

import java.lang.reflect.Field;
import java.util.ArrayList;

import static org.junit.Assert.*;

/**
* Department Tester. 
* 
* @author <Authors name> 
* @since <pre>Jan 28, 2020</pre> 
* @version 1.0 
*/ 
public class DepartmentTest { 

@Before
public void before() throws Exception {
} 

@After
public void after() throws Exception { 
}


/** 
* 
* Method: getName() 
* 
*/ 
@Test
public void testGetName() throws Exception {
    //given
    final Department department = new Department("personal");
    final Field field = department.getClass().getDeclaredField("name");
    field.setAccessible(true);
    field.set(department, "test");

    //when
    final String result = department.getName();

    //then
    assertEquals("field wasn't retrieved properly", result, "test");
} 

/** 
* 
* Method: getPersonInCharge() 
* 
*/ 
@Test
public void testGetPersonInCharge() throws Exception {
    //given
    final Department department = new Department("personal");
    User user = new User("peter", "smith", "login", "password", Role.ROLE_DEPARTMENT_MANAGER);
    final Field field = department.getClass().getDeclaredField("personInCharge");
    field.setAccessible(true);
    field.set(department, user);

    //when
    final User result = department.getPersonInCharge();

    //then
    assertEquals("field wasn't retrieved properly", result, user);
} 

/** 
* 
* Method: getProducts() 
* 
*/ 
@Test
public void testGetProducts() throws Exception {
    final Department department = new Department("personal");
    final Field field = department.getClass().getDeclaredField("products");
    field.setAccessible(true);
    ArrayList<Product> test1 = new ArrayList<>();
    for(int i = 0; i < 4; i++){
        test1.add(new Product(1, "product"+Integer.toString(i),"good"));
    }
    field.set(department, test1);

    final ArrayList<Product> result = department.getProducts();

    assertArrayEquals("field wasn't retrieved properly", test1.toArray(),result.toArray());
} 


/** 
* 
* Method: setPersonInCharge(User personInCharge) 
* 
*/ 
@Test
public void testSetPersonInCharge() throws Exception {
    final Department department = new Department("personal");
    User user = new User("testfirst", "testlast","login1","password", Role.ROLE_DEPARTMENT_MANAGER);
    department.setPersonInCharge(user);
    final Field field = department.getClass().getDeclaredField("personInCharge");
    field.setAccessible(true);
    assertEquals("Fields didn't match", field.get(department), user);
} 

/** 
* 
* Method: setProducts(ArrayList<Product> products) 
* 
*/ 
@Test
public void testSetProducts() throws Exception {
    ArrayList<Product> test1 = new ArrayList<>();
    for(int i = 0; i < 4; i++){
        test1.add(new Product(i, "product"+Integer.toString(i),"good"));
    }
    final Department department = new Department("shopping");
    department.setProducts(test1);
    final Field field = department.getClass().getDeclaredField("products");
    field.setAccessible(true);
    assertEquals("Fields didn't match", field.get(department), test1);
}

/** 
* 
* Method: toString() 
* 
*/ 
@Test
public void testToString() throws Exception {
    User user = new User("firstname", "lastname", "login1", "password", Role.ROLE_DEPARTMENT_MANAGER);
    ArrayList<Product> products = new ArrayList<>();
    for(int i = 0; i < 4; i++){
        products.add(new Product(i, "product"+Integer.toString(i),"good"));
    }
    Department department = new Department("name");
    department.setProducts(products);
    department.setPersonInCharge(user);
    String text1 = "<" + department.getName() + "," + department.getPersonInCharge() + "," + department.getProducts() + ">";
    assertEquals("<name," + user.toString() + "," + products + ">", text1);

} 

/** 
* 
* Method: hashCode() 
* 
*/ 
@Test
public void testHashCode() throws Exception {
    User user = new User("firstname", "lastname", "login1", "password",
            Role.ROLE_DEPARTMENT_MANAGER);
    ArrayList<Product> products = new ArrayList<>();
    for(int i = 0; i < 4; i++){
        products.add(new Product(i, "product"+Integer.toString(i),"good"));
    }
    Department department = new Department("name");
    department.setProducts(products);
    department.setPersonInCharge(user);
} 

/** 
* 
* Method: equals(Object obj) 
* 
*/ 
@Test
public void testEquals() throws Exception {
    CompanyExample companyExample = new CompanyExample();
    Company company = companyExample.getCompanyExample();
    Department department1 = company.getStores().get(0).getDepartments().get(0);
    Department department2 = company.getStores().get(0).getDepartments().get(1);
    Department department3 = company.getStores().get(1).getDepartments().get(0);
    Department department4 = company.getStores().get(1).getDepartments().get(1);
    Department department5 = company.getStores().get(0).getDepartments().get(0);
    assertFalse(department1.equals(department2));
    assertFalse(department1.equals(department3));
    assertFalse(department1.equals(department4));
    assertTrue(department1.equals(department5));

} 


} 
