package test.magsport.model; 

import magsport.model.*;
import org.junit.Test;
import org.junit.Before; 
import org.junit.After;
import test.magsport.CompanyExample;

import java.lang.reflect.Field;
import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Store Tester. 
 * 
 * @author <Authors name> 
 * @since <pre>Jan 28, 2020</pre> 
 * @version 1.0 
 */ 
public class StoreTest { 

	@Before
	public void before() throws Exception { 
	} 

	@After
	public void after() throws Exception { 
	}

	/** 
	 * 
	 * Method: getPersonInCharge() 
	 * 
	 */ 
	@Test
	public void testGetPersonInCharge() throws Exception {
		//given
		final Store store = new Store("nike1", "shanghai", "renmin Road");
		final Field field = store.getClass().getDeclaredField("personInCharge");
		field.setAccessible(true);
		User user = new User("peter", "smith", "login", "passoword", Role.ROLE_STORE_MANAGER);
		field.set(store, user);

		//when
		final User result = store.getPersonInCharge();

		//then
		assertEquals("field wasn't retrieved properly", result, user);
	} 

	/** 
	 * 
	 * Method: getDepartments() 
	 * 
	 */ 
	@Test
	public void testGetDepartments() throws Exception {
		final Store store = new Store("nike", "shanghai", "renmin Road");
		final Field field = store.getClass().getDeclaredField("departments");
		field.setAccessible(true);
		ArrayList<Department> test1 = new ArrayList<>();
		for(int i = 0; i < 4; i++){
			test1.add(new Department(Integer.toString(i)));
		}
		field.set(store, test1);

		final ArrayList<Department> result = store.getDepartments();

		assertArrayEquals("field wasn't retrieved properly", test1.toArray(),result.toArray());
	} 

	/** 
	 * 
	 * Method: getUsers() 
	 * 
	 */ 
	@Test
	public void testGetUsers() throws Exception {
		//given
		final Store store = new Store("nike", "shanghai", "renmin Road");
		final Field field = store.getClass().getDeclaredField("users");
		field.setAccessible(true);
		ArrayList<User> test1 = new ArrayList<>();
		for(int i = 0; i < 4; i++){
			test1.add(new User("firstname"+Integer.toString(i),"lastname"+Integer.toString(i), "Login"+Integer.toString(i), "password"+Integer.toString(i),Role.ROLE_DEPARTMENT_MANAGER));
		}
		field.set(store, test1);

		//when
		final ArrayList<User> result = store.getUsers();

		//then
		assertArrayEquals("field wasn't retrieved properly", test1.toArray(),result.toArray());
	}

	/** 
	 * 
	 * Method: setPersonInCharge(User personInCharge) 
	 * 
	 */ 
	@Test
	public void testSetPersonInCharge() throws Exception {
		final Store store = new Store("nike", "shanghai", "renmin Road");
		User user = new User("peter","smith","login", "password", Role.ROLE_STORE_MANAGER);
		store.setPersonInCharge(user);
		final Field field = store.getClass().getDeclaredField("personInCharge");
		field.setAccessible(true);
		assertEquals("Fields didn't match", field.get(store), user);
	} 

	/** 
	 * 
	 * Method: setDepartments(ArrayList<Department> departments) 
	 * 
	 */ 
	@Test
	public void testSetDepartments() throws Exception {
		ArrayList<Department> test1 = new ArrayList<>();
		for(int i = 0; i < 4; i++){
			test1.add(new Department(Integer.toString(i)));
		}
		final Store store = new Store("nike", "shanghai", "renmin Road");
		store.setDepartments(test1);
		final Field field = store.getClass().getDeclaredField("departments");
		field.setAccessible(true);
		assertEquals("Fields didn't match", field.get(store), test1);
	} 


	/** 
	 * 
	 * Method: setUsers(ArrayList<User> users) 
	 * 
	 */ 
	@Test
	public void testSetUsers() throws Exception {
		ArrayList<User> test1 = new ArrayList<>();
		for(int i = 0; i < 4; i++){
			test1.add(new User("fname"+Integer.toString(i), "lname"+
					Integer.toString(i), "login"+ Integer.toString(i), "pwd"+
							Integer.toString(i), Role.ROLE_DEPARTMENT_MANAGER));
		}
		final Store store = new Store("store1","city1", "address1");
		store.setUsers(test1);
		final Field field = store.getClass().getDeclaredField("users");
		field.setAccessible(true);
		assertEquals("Fields didn't match", field.get(store), test1);
	}

	/** 
	 * 
	 * Method: getUser(String login) 
	 * 
	 */ 
	@Test
	public void testGetUser() throws Exception {
		CompanyExample companyExample = new CompanyExample();
		Company company = companyExample.getCompanyExample();
		Store store1 = companyExample.getCompanyExample().getStores().get(0);
		Store store2 = companyExample.getCompanyExample().getStores().get(1);
		Store store3 = companyExample.getCompanyExample().getStores().get(2);
		User user1 = store1.getUsers().get(0);
		User user2 = store2.getUsers().get(0);
		User user3 = store3.getUsers().get(0);
		assertEquals(user1, store1.getUser("dylan.motard"));
		assertEquals(user2, store2.getUser("monique.leloup"));
		assertEquals(user3, store3.getUser("bernadette.fifou"));
	}

	/** 
	 * 
	 * Method: equals(Object obj) 
	 * 
	 */ 
	@Test
	public void testEquals() throws Exception {
		CompanyExample companyExample = new CompanyExample();
		Store store1 = companyExample.getCompanyExample().getStores().get(0);
		Store store2 = companyExample.getCompanyExample().getStores().get(1);
		Store store3 = companyExample.getCompanyExample().getStores().get(2);
		Store store4 = companyExample.getCompanyExample().getStores().get(0);
		assertTrue(store1.equals(store4));
		assertFalse(store1.equals(store2));
		assertFalse(store1.equals(store3));
	} 


} 
