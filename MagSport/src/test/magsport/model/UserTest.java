package test.magsport.model;

import magsport.model.Role;
import magsport.model.User;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;

import java.security.NoSuchAlgorithmException;
import static org.junit.Assert.*;



public class UserTest {

	@Before
	public void before() throws Exception {
	}

	@After
	public void after() throws Exception {
	}

	/**
	 * 
	 * Method: hash(String password)
	 * 
	 */
	@Test
	public void testHash() throws Exception {
		User user1 = new User("firstname1", "lastname1", "login1", "password1", Role.ROLE_DEPARTMENT_MANAGER);
		User user2 = new User("firstname2", "lastname2", "login2", "password2", Role.ROLE_DEPARTMENT_MANAGER);
		User user3 = new User("firstname3", "lastname3", "login3", "password3", Role.ROLE_CEO);
		try {
			assertTrue(
					"b14d501a594442a01c6859541bcb3e8164d183d32937b851835442f69d5c94e".equals(user1.hash("password1")));
			assertTrue(
					"6cf615d5bcaac778352a8f1f3360d23f02f34ec182e259897fd6ce485d7870d4".equals(user2.hash("password2")));
			assertTrue(
					"5906ac361a137e2d286465cd6588ebb5ac3f5ae955001100bc41577c3d751764".equals(user3.hash("password3")));

		} catch (NoSuchAlgorithmException e) {
			System.out.println("we can not find the exactly algorithme!");
		}

	}

	/**
	 * 
	 * Method: authentication(String password)
	 * 
	 */
	@Test
	public void testAuthentication() throws Exception {
		User user1 = new User("firstname1", "lastname1", "login1", "password1", Role.ROLE_DEPARTMENT_MANAGER);
		User user2 = new User("firstname2", "lastname2", "login2", "password2", Role.ROLE_DEPARTMENT_MANAGER);
		User user3 = new User("firstname3", "lastname3", "login3", "password3", Role.ROLE_CEO);
		assertTrue(user1.authentication("password1"));
		assertTrue(user2.authentication("password2"));
		assertFalse(user3.authentication("password1"));
	}

	/**
	 * 
	 * Method: toString()
	 * 
	 */
	@Test
	public void testToString() throws Exception {

		User[] users = { new User("firstname1", "lastname1", "login1", "password1", Role.ROLE_DEPARTMENT_MANAGER),
				new User("firstname2", "lastname2", "login2", "password2", Role.ROLE_DEPARTMENT_MANAGER),
				new User("firstname3", "lastname3", "login3", "password3", Role.ROLE_CEO) };
		String result;
		for (User user : users) {
			result = "<" + user.getFirstName() + " " + user.getLastName() + "," + user.getRole().toString() + ">";
			assertTrue(result.equals(user.toString()));
		}
	}

	/**
	 * 
	 * Method: equals(Object obj)
	 * 
	 */
	@Test
	public void testEquals() throws Exception {
		User user1 = new User("firstname1", "lastname1", "login1", "password1", Role.ROLE_DEPARTMENT_MANAGER);
		User user2 = new User("firstname2", "lastname2", "login2", "password2", Role.ROLE_DEPARTMENT_MANAGER);
		User user3 = new User("firstname3", "lastname3", "login3", "password3", Role.ROLE_CEO);
		User user4 = new User("firstname1", "lastname1", "login1", "password1", Role.ROLE_DEPARTMENT_MANAGER);
		assertTrue(user1.equals(user4));
		assertFalse(user2.equals(user1));
		assertFalse(user3.equals(user1));
	}

}
