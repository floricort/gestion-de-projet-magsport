package test.magsport.model; 

import magsport.model.Product;
import org.junit.Test;
import org.junit.Before; 
import org.junit.After;

import java.lang.reflect.Field;

import static org.junit.Assert.*;

/**
* Product Tester. 
* 
* @author <Authors name> 
* @since <pre>Jan 28, 2020</pre> 
* @version 1.0 
*/ 
public class ProductTest { 

@Before
public void before() throws Exception {

} 

@After
public void after() throws Exception { 
} 

/** 
* 
* Method: getStock() 
* 
*/ 
@Test
public void testGetStock() throws Exception {
    //given
    final Product product = new Product(1, "basketball", "good");
    final Field field = product.getClass().getDeclaredField("stock");
    field.setAccessible(true);
    field.set(product, 10);

    //when
    final int result = product.getStock();

    //then
    assertEquals("field wasn't retrieved properly", result, 10);
} 


/** 
* 
* Method: setStock(Integer stock) 
* 
*/ 
@Test
public void testSetStock() throws Exception {
    final Product product = new Product(1, "basketball", "good");
    product.setStock(10);
    final Field field = product.getClass().getDeclaredField("stock");
    field.setAccessible(true);
    assertEquals("Fields didn't match", field.get(product), 10);
} 

/** 
* 
* Method: toString() 
* 
*/ 
@Test
public void testToString() throws Exception {
    Product product1 = new Product(1,"product1", "good1");
    product1.setStock(1);
    Product product2 = new Product(2,"product2", "good2");
    product2.setStock(2);
    Product product3 = new Product(3,"product3", "good3");
    product3.setStock(3);
    assertEquals("<1,product1,good1,1>", product1.toString());
    assertEquals("<2,product2,good2,2>", product2.toString());
    assertEquals("<3,product3,good3,3>", product3.toString());
} 

/** 
* 
* Method: hashCode() 
* 
*/ 
@Test
public void testHashCode() throws Exception {
    Product product1 = new Product(1,"product1", "good1");
    product1.setStock(1);
    Product product2 = new Product(2,"product2", "good2");
    product2.setStock(2);
    Product product3 = new Product(3,"product3", "good3");
    product3.setStock(3);
    assertEquals(1070239437, product1.hashCode());
    assertEquals(1070270221, product2.hashCode());
    assertEquals(1070301005, product3.hashCode());
} 

/** 
* 
* Method: equals(Object obj) 
* 
*/ 
@Test
public void testEquals() throws Exception {
    Product product1 = new Product(1,"product1", "good1");
    product1.setStock(1);
    Product product2 = new Product(2,"product2", "good2");
    product2.setStock(2);
    Product product3 = new Product(3,"product3", "good3");
    product3.setStock(3);
    Product product4 = new Product(1,"product1", "good1");
    assertFalse(product1.equals(product2));
    assertFalse(product1.equals(product3));
    assertTrue(product1.equals(product1));
    assertFalse(product1.equals(product4));
} 


} 
