package magsport.view;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import magsport.model.Department;
import magsport.model.User;

public class TableAisel extends AbstractTableModel {

	final String[] columnsTitle = { "Nom", "Responssable" };
	private ArrayList<Department> listDepartment;
	private ArrayList<ArrayList<Object>> tableValues;

	/**
	 * Constructor
	 */
	public TableAisel(ArrayList<Department> listDepartment) {
		tableValues = new ArrayList<ArrayList<Object>>();
		if (listDepartment == null)
			this.listDepartment = new ArrayList<Department>();
		else
			this.listDepartment = listDepartment;
		refresh();
	}

	/**
	 * Return the number of columns
	 * 
	 * @return the number of columns
	 */
	@Override
	public int getColumnCount() {
		return columnsTitle.length;
	}

	/**
	 * Return the number of rows
	 * 
	 * @return the number of rows
	 */
	public int getRowCounter() {
		return tableValues.size();
	}

	/**
	 * Return the class type of the column whose the index is pass in parameter
	 * 
	 * @param columnIndex the index of the column
	 * @return the class of the column
	 */
	@Override
	public Class getColumnClass(int columnIndex) {
		if (columnIndex >= getColumnCount()) {
			throw new IndexOutOfBoundsException();
		}
		// return tableValues.get(0).get(columnIndex).getClass();
		switch (columnIndex) {
		case 0:
			return String.class;
		case 1:
			return User.class;
		default:
			return String.class;
		}

	}

	/**
	 * Return the name of the column pass in parameter
	 * 
	 * @param columnIndex the index
	 * @return the name of the column
	 */
	@Override
	public String getColumnName(int columnIndex) throws IndexOutOfBoundsException {
		if (columnIndex >= getColumnCount()) {
			throw new IndexOutOfBoundsException();
		}
		return columnsTitle[columnIndex];
	}

	/**
	 * Return the number of rows in table
	 * 
	 * @return the number of rows
	 */
	@Override
	public int getRowCount() {
		return tableValues.size();
	}

	/**
	 * Return the value at the position pass in parameter
	 * 
	 * @param rowIndex    row index of table
	 * @param columnIndex row index of table
	 */
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) throws IndexOutOfBoundsException {
		if (rowIndex >= getRowCount() || columnIndex >= getColumnCount()) {
			throw new IndexOutOfBoundsException();
		}
		return tableValues.get(rowIndex).get(columnIndex);
	}

	/**
	 * Return if the cell is editable
	 * 
	 * @param rowIndex    row index of table
	 * @param columnIndex row index of table
	 * @return if the cell is editable
	 */
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) throws IndexOutOfBoundsException {
		if (rowIndex >= getRowCount() || columnIndex >= getColumnCount()) {
			throw new IndexOutOfBoundsException();
		}
		return false;
	}

	/**
	 * set listLocal
	 */
	public void setList(ArrayList<Department> list) {
		this.listDepartment.clear();
		this.listDepartment.addAll(list);
		refresh();
	}

	public void addToList(Department department) {
		listDepartment.add(department);
		refresh();
	}

	public void suprToList(Department department) {
		listDepartment.remove(department);
		refresh();
	}

	/**
	 * Refresh the table
	 */
	public void refresh() {
		tableValues.clear();
		for (Department department : listDepartment) {
			ArrayList<Object> newRow = new ArrayList<Object>();
			newRow.add(department.getName());
			newRow.add(department.getPersonInCharge());
			tableValues.add(newRow);
		}
		fireTableDataChanged();
	}

}
