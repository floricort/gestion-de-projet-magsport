package magsport.view;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Logger;

import javax.swing.*;
import javax.swing.border.MatteBorder;

import magsport.controller.ProductAddEditController;
import magsport.model.Department;
import magsport.model.Product;
import magsport.model.Store;

public class ProductAddEditView extends JPanel {

	private MainFrame mainFrame;
	private JTextField txtName;
	private JTextField txtCode;
	private JTextField textStock;
	private ProductAddEditController productAddEditController;
	private Department department;
	private Store store;

	/**
	 * Create the panel.
	 * @param mainFrame 
	 * @param productAddEditController 
	 */
	public ProductAddEditView(MainFrame mainFrame, ProductAddEditController productAddEditController) {
		this.productAddEditController = productAddEditController;
		this.mainFrame = mainFrame;
		setLayout(new BorderLayout(0, 0));

		JPanel panelHeadTable = new JPanel();
		add(panelHeadTable, BorderLayout.CENTER);
		panelHeadTable.setBorder(new MatteBorder(2, 2, 2, 2, (Color) new Color(0, 0, 0)));
		GridBagLayout gbl_panelHeadTable = new GridBagLayout();
		gbl_panelHeadTable.rowHeights = new int[] { 0, 0, 70, 0, 0 };
		gbl_panelHeadTable.columnWidths = new int[] { 200, 200 };
		gbl_panelHeadTable.columnWeights = new double[] { 1.0, 1.0 };
		gbl_panelHeadTable.rowWeights = new double[] { 1.0, 1.0, 1.0, 1.0, 1.0 };
		panelHeadTable.setLayout(gbl_panelHeadTable);

		JLabel lblName = new JLabel("Nom : ");
		GridBagConstraints gbc_lblName = new GridBagConstraints();
		gbc_lblName.anchor = GridBagConstraints.EAST;
		gbc_lblName.insets = new Insets(0, 0, 5, 5);
		gbc_lblName.gridx = 0;
		gbc_lblName.gridy = 0;
		panelHeadTable.add(lblName, gbc_lblName);

		txtName = new JTextField();
		GridBagConstraints gbc_txtName = new GridBagConstraints();
		gbc_txtName.anchor = GridBagConstraints.WEST;
		gbc_txtName.insets = new Insets(0, 0, 5, 0);
		gbc_txtName.gridx = 1;
		gbc_txtName.gridy = 0;
		panelHeadTable.add(txtName, gbc_txtName);
		txtName.setText("Nom");
		txtName.setColumns(10);

		JLabel lblCode = new JLabel("Code :");
		GridBagConstraints gbc_lblCode = new GridBagConstraints();
		gbc_lblCode.anchor = GridBagConstraints.EAST;
		gbc_lblCode.insets = new Insets(0, 0, 5, 5);
		gbc_lblCode.gridx = 0;
		gbc_lblCode.gridy = 1;
		panelHeadTable.add(lblCode, gbc_lblCode);

		txtCode = new JTextField();
		GridBagConstraints gbc_txtCode = new GridBagConstraints();
		gbc_txtCode.anchor = GridBagConstraints.WEST;
		gbc_txtCode.insets = new Insets(0, 0, 5, 0);
		gbc_txtCode.gridx = 1;
		gbc_txtCode.gridy = 1;
		panelHeadTable.add(txtCode, gbc_txtCode);
		txtCode.setText("152148");
		txtCode.setColumns(10);

		JLabel lblDescription = new JLabel("Description :");
		GridBagConstraints gbc_lblDescription = new GridBagConstraints();
		gbc_lblDescription.anchor = GridBagConstraints.EAST;
		gbc_lblDescription.insets = new Insets(0, 0, 5, 5);
		gbc_lblDescription.gridx = 0;
		gbc_lblDescription.gridy = 2;
		panelHeadTable.add(lblDescription, gbc_lblDescription);

		JScrollPane scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.insets = new Insets(0, 0, 5, 0);
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 1;
		gbc_scrollPane.gridy = 2;
		panelHeadTable.add(scrollPane, gbc_scrollPane);

		JTextPane txtpnTest = new JTextPane();
		scrollPane.setViewportView(txtpnTest);
		txtpnTest.setText("test");

		JLabel lblStock = new JLabel("Stock :");
		GridBagConstraints gbc_lblStock = new GridBagConstraints();
		gbc_lblStock.anchor = GridBagConstraints.EAST;
		gbc_lblStock.insets = new Insets(0, 0, 5, 5);
		gbc_lblStock.gridx = 0;
		gbc_lblStock.gridy = 3;
		panelHeadTable.add(lblStock, gbc_lblStock);

		textStock = new JTextField();
		GridBagConstraints gbc_textStock = new GridBagConstraints();
		gbc_textStock.insets = new Insets(0, 0, 5, 0);
		gbc_textStock.anchor = GridBagConstraints.WEST;
		gbc_textStock.gridx = 1;
		gbc_textStock.gridy = 3;
		panelHeadTable.add(textStock, gbc_textStock);
		textStock.setText("15");
		textStock.setColumns(10);

		JPanel panel = new JPanel();
		add(panel, BorderLayout.NORTH);

		JLabel lblProduct = new JLabel("Product");
		panel.add(lblProduct);

		JPanel panel_1 = new JPanel();
		add(panel_1, BorderLayout.SOUTH);

		JButton btnCancel = new JButton("Annuler");
		panel_1.add(btnCancel);

		JButton btnAdd = new JButton("Ajouter");
		panel_1.add(btnAdd);

		JButton btnDelete = new JButton("Supprimer");
		panel_1.add(btnDelete);
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				deleteProduct(txtCode.getText(), textStock.getText());
			}
		});
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addProduct(txtName.getText(), txtCode.getText(), txtpnTest.getText(), textStock.getText());
				mainFrame.displayAisleView(department, store);
			}
		});
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mainFrame.displayAisleView(department, store);
			}
		});
	}

	public void resultShow(String result){
		JOptionPane.showMessageDialog(null, result, "Erreur",
				JOptionPane.INFORMATION_MESSAGE);
	}

	public void addProduct(String name, String code, String description, String stock){
		if(productAddEditController.ProductAdd(name, code,description,stock,department)){
			resultShow("you have add the product successfully.");
		}
		else{
			resultShow("Something is wrong.");
		}
	}

	public void deleteProduct(String code, String stock){
		if(productAddEditController.ProductDelete(code, stock)){
			resultShow("you have delete the product successfully.");
		}
		else {
			resultShow("sorry! Please enter the correct stock!");
		}
	}


	public void refresh(Product product, Department department, Store store) {
		this.department = department;
		this.store = store;
		// TODO if product is null : create a new product
	}

}
