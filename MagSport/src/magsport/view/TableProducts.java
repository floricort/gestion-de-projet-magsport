package magsport.view;

import magsport.model.Department;
import magsport.model.Product;
import magsport.model.Store;
import magsport.model.User;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;

public class TableProducts extends AbstractTableModel {

	final String[] columnsTitle = { "idProduct", "name", "stock" };// TODO
	private ArrayList<Product> listProducts;
	private ArrayList<ArrayList<Object>> tableValues;

	/**
	 * Constructor
	 */
	public TableProducts(ArrayList<Product> listProducts) {
		tableValues = new ArrayList<ArrayList<Object>>();
		if (listProducts == null)
			this.listProducts = new ArrayList<Product>();
		else
			this.listProducts = listProducts;
		refresh();
	}

	/**
	 * Return the number of columns
	 * 
	 * @return the number of columns
	 */
	@Override
	public int getColumnCount() {
		return columnsTitle.length;
	}

	/**
	 * Return the number of rows
	 * 
	 * @return the number of rows
	 */
	public int getRowCounter() {
		return tableValues.size();
	}

	/**
	 * Return the class type of the column whose the index is pass in parameter
	 * 
	 * @param columnIndex the index of the column
	 * @return the class of the column
	 */
	@Override
	public Class getColumnClass(int columnIndex) {
		if (columnIndex >= getColumnCount()) {
			throw new IndexOutOfBoundsException();
		}
		// return tableValues.get(0).get(columnIndex).getClass();
		switch (columnIndex) {
		case 0:
			return Integer.class;
		case 1:
			return String.class;
		case 2:
			return Integer.class;
		default:
			return String.class;
		}

	}

	/**
	 * Return the name of the column pass in parameter
	 * 
	 * @param columnIndex the index
	 * @return the name of the column
	 */
	@Override
	public String getColumnName(int columnIndex) throws IndexOutOfBoundsException {
		if (columnIndex >= getColumnCount()) {
			throw new IndexOutOfBoundsException();
		}
		return columnsTitle[columnIndex];
	}

	/**
	 * Return the number of rows in table
	 * 
	 * @return the number of rows
	 */
	@Override
	public int getRowCount() {
		return tableValues.size();
	}

	/**
	 * Return the value at the position pass in parameter
	 * 
	 * @param rowIndex    row index of table
	 * @param columnIndex row index of table
	 */
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) throws IndexOutOfBoundsException {
		if (rowIndex >= getRowCount() || columnIndex >= getColumnCount()) {
			throw new IndexOutOfBoundsException();
		}
		return tableValues.get(rowIndex).get(columnIndex);
	}

	/**
	 * Return if the cell is editable
	 * 
	 * @param rowIndex    row index of table
	 * @param columnIndex row index of table
	 * @return if the cell is editable
	 */
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) throws IndexOutOfBoundsException {
		if (rowIndex >= getRowCount() || columnIndex >= getColumnCount()) {
			throw new IndexOutOfBoundsException();
		}
		return false;
	}

	/**
	 * set listLocal
	 */
	public void setList(ArrayList<Product> list) {
		this.listProducts.clear();
		this.listProducts.addAll(list);
		refresh();
	}

	public void addToList(Product product) {
		listProducts.add(product);
		refresh();
	}

	public void suprToList(Product product) {
		listProducts.remove(product);
		refresh();
	}

	/**
	 * Refresh the table
	 */
	public void refresh() {
		tableValues.clear();
		for (Product product : listProducts) {
			ArrayList<Object> newRow = new ArrayList<Object>();
			newRow.add(product.getIdProduct());
			newRow.add(product.getName());
			//newRow.add(product.getDescription());
			newRow.add(product.getStock());
			tableValues.add(newRow);
		}
		fireTableDataChanged();
	}

}
