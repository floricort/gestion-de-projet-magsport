package magsport.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.border.MatteBorder;

import magsport.controller.AiselAddEditController;
import magsport.controller.AisleController;
import magsport.model.Department;
import magsport.model.Store;
import magsport.model.User;

public class AiselAddEditView extends JPanel {

	private MainFrame mainFrame;
	private JTextField txtName;
	private JComboBox<User> comboBoxManager;
	private JButton btnAdd;
	private AiselAddEditController aiselAddEditController;
	private Department department;
	private Store store;

	/**
	 * Create the panel.
	 * @param mainFrame 
	 * @param aiselAddEditController 
	 */
	public AiselAddEditView(MainFrame mainFrame, AiselAddEditController aiselAddEditController) {
		this.aiselAddEditController = aiselAddEditController;
		this.mainFrame = mainFrame;
		setLayout(new BorderLayout(0, 0));

		JPanel panelHeadTable = new JPanel();
		add(panelHeadTable, BorderLayout.CENTER);
		panelHeadTable.setBorder(new MatteBorder(2, 2, 2, 2, (Color) new Color(0, 0, 0)));
		GridBagLayout gbl_panelHeadTable = new GridBagLayout();
		gbl_panelHeadTable.rowHeights = new int[] { 0, 0, 0, 0 };
		gbl_panelHeadTable.columnWidths = new int[] { 200, 200 };
		gbl_panelHeadTable.columnWeights = new double[] { 1.0, 1.0 };
		gbl_panelHeadTable.rowWeights = new double[] { 1.0, 1.0, 1.0, 1.0 };
		panelHeadTable.setLayout(gbl_panelHeadTable);

		JLabel lblName = new JLabel("Nom : ");
		GridBagConstraints gbc_lblName = new GridBagConstraints();
		gbc_lblName.anchor = GridBagConstraints.EAST;
		gbc_lblName.insets = new Insets(0, 0, 5, 5);
		gbc_lblName.gridx = 0;
		gbc_lblName.gridy = 1;
		panelHeadTable.add(lblName, gbc_lblName);

		txtName = new JTextField();
		GridBagConstraints gbc_txtName = new GridBagConstraints();
		gbc_txtName.insets = new Insets(0, 0, 5, 0);
		gbc_txtName.anchor = GridBagConstraints.WEST;
		gbc_txtName.gridx = 1;
		gbc_txtName.gridy = 1;
		panelHeadTable.add(txtName, gbc_txtName);
		txtName.setText("Nom");
		txtName.setColumns(10);

		JLabel lblManager = new JLabel("Responsable :");
		GridBagConstraints gbc_lblManager = new GridBagConstraints();
		gbc_lblManager.anchor = GridBagConstraints.EAST;
		gbc_lblManager.insets = new Insets(0, 0, 5, 5);
		gbc_lblManager.gridx = 0;
		gbc_lblManager.gridy = 2;
		panelHeadTable.add(lblManager, gbc_lblManager);

		comboBoxManager = new JComboBox();
		GridBagConstraints gbc_comboBoxManager = new GridBagConstraints();
		gbc_comboBoxManager.insets = new Insets(0, 0, 5, 0);
		gbc_comboBoxManager.anchor = GridBagConstraints.WEST;
		gbc_comboBoxManager.gridx = 1;
		gbc_comboBoxManager.gridy = 2;
		panelHeadTable.add(comboBoxManager, gbc_comboBoxManager);

		JPanel panelBtnMenu = new JPanel();
		add(panelBtnMenu, BorderLayout.SOUTH);
		panelBtnMenu.setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));

		JButton btnCancel = new JButton("Annuler");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mainFrame.displayStoreView(store);
			}
		});
		panelBtnMenu.add(btnCancel);

		btnAdd = new JButton("Ajouter");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (department == null){
					aiselAddEditController.addNewDepartment(txtName.getText(),store);
					department = aiselAddEditController.findDepartment(txtName.getText(),store);
					mainFrame.displayAisleView(department,store);
				}else {
					department.setName(txtName.getText());
					department.setPersonInCharge((User)comboBoxManager.getSelectedItem());
					mainFrame.displayAisleView(department,store);
				}

			}
		});
		panelBtnMenu.add(btnAdd);

		JButton btnDelete = new JButton("Supprimer");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int choice = JOptionPane.showConfirmDialog(null, "Supprimer ce rayon suprimera tout ses rayon, produit, Employer", "Attention", JOptionPane.OK_CANCEL_OPTION);
                if (choice == 0) {
					department = aiselAddEditController.findDepartment(txtName.getText(),store);
					aiselAddEditController.deleteDepartment(department,store);
					mainFrame.displayStoreView(store);
				}
			}
		});
		panelBtnMenu.add(btnDelete);

		JPanel panelTitleInformation = new JPanel();
		add(panelTitleInformation, BorderLayout.NORTH);
		panelTitleInformation.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		JLabel lblAisel = new JLabel("Aisel");
		panelTitleInformation.add(lblAisel);
		}


	public void refresh(Department department, Store store) {
		// TODO if department is null : cr�ation d'un nouveaux departement.
		this.department = department;
		this.store = store;
		if (department == null){
			txtName.setText("");
			comboBoxManager.removeAllItems();
			comboBoxManager.setEnabled(false);
			btnAdd.setText("Ajouter");
		}else{
			txtName.setText(department.getName());
			comboBoxManager.removeAllItems();
			comboBoxManager.setEnabled(true);
			btnAdd.setText("Modifier");
			ArrayList<User> listManager = aiselAddEditController.getPossibleManager(store);
			for (User user : listManager) {
				comboBoxManager.addItem(user);
			}
			comboBoxManager.setSelectedItem(department.getPersonInCharge());
		}

	}
}
