package magsport.view;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import magsport.controller.EmployeesController;
import magsport.model.Store;
import magsport.model.User;

public class TableEmployees extends AbstractTableModel {

	final String[] columnsTitle = {"Nom magasin", "Implantation", "Responsabilité", "Prénom Nom"};
	private ArrayList<User> listEmployees;
	private ArrayList<ArrayList<Object>> tableValues;
	private EmployeesController employeesController;

	/**
	 * Constructor
	 */
	public TableEmployees(ArrayList<User> listEmployees, EmployeesController employeesController) {
		this.employeesController = employeesController; 
		tableValues = new ArrayList<ArrayList<Object>>();
		if (listEmployees == null)
			this.listEmployees = new ArrayList<User>();
		else
			this.listEmployees = listEmployees;
		refresh();
	}

	/**
	 * Return the number of columns
	 * 
	 * @return the number of columns
	 */
	@Override
	public int getColumnCount() {
		return columnsTitle.length;
	}

	/**
	 * Return the number of rows
	 * 
	 * @return the number of rows
	 */
	public int getRowCounter() {
		return tableValues.size();
	}

	/**
	 * Return the class type of the column whose the index is pass in parameter
	 * 
	 * @param columnIndex the index of the column
	 * @return the class of the column
	 */
	@Override
	public Class getColumnClass(int columnIndex) {
		if (columnIndex >= getColumnCount()) {
			throw new IndexOutOfBoundsException();
		}
		return String.class;
	}

	/**
	 * Return the name of the column pass in parameter
	 * 
	 * @param columnIndex the index
	 * @return the name of the column
	 */
	@Override
	public String getColumnName(int columnIndex) throws IndexOutOfBoundsException {
		if (columnIndex >= getColumnCount()) {
			throw new IndexOutOfBoundsException();
		}
		return columnsTitle[columnIndex];
	}

	/**
	 * Return the number of rows in table
	 * 
	 * @return the number of rows
	 */
	@Override
	public int getRowCount() {
		return tableValues.size();
	}

	/**
	 * Return the value at the position pass in parameter
	 * 
	 * @param rowIndex    row index of table
	 * @param columnIndex row index of table
	 */
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) throws IndexOutOfBoundsException {
		if (rowIndex >= getRowCount() || columnIndex >= getColumnCount()) {
			throw new IndexOutOfBoundsException();
		}
		return tableValues.get(rowIndex).get(columnIndex);
	}

	/**
	 * Return if the cell is editable
	 * 
	 * @param rowIndex    row index of table
	 * @param columnIndex row index of table
	 * @return if the cell is editable
	 */
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) throws IndexOutOfBoundsException {
		if (rowIndex >= getRowCount() || columnIndex >= getColumnCount()) {
			throw new IndexOutOfBoundsException();
		}
		return false;
	}

	/**
	 * set listLocal
	 */
	public void setList(ArrayList<User> list) {
		this.listEmployees.clear();
		this.listEmployees.addAll(list);
		refresh();
	}

	public void addToList(User user) {
		listEmployees.add(user);
		refresh();
	}

	public void suprToList(User user) {
		listEmployees.remove(user);
		refresh();
	}

	/**
	 * Refresh the table
	 */
	public void refresh() {
		tableValues.clear();
		for (User user : listEmployees) {
			ArrayList<Object> newRow = new ArrayList<Object>();
			Store store = employeesController.getStoreOfEmployee(user);
			newRow.add(store.getName()); // TODO
			newRow.add(store.getAddress() + "," + store.getCity()); // TODO
			newRow.add(user.getRole());
			newRow.add(user.getFirstName() + " " + user.getLastName());
			tableValues.add(newRow);
		}
		fireTableDataChanged();
	}

	/*
	 * Return the selected user or null
	 */
	public User getUserByIndex(Integer index) {
		if (index >= tableValues.size() || index >= listEmployees.size()) {
			return null;
		}
		return listEmployees.get(index);
	}
}
