package magsport.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.border.MatteBorder;

import magsport.controller.EmployeeController;
import magsport.model.Role;
import magsport.model.Store;
import magsport.model.User;

import java.awt.Color;

public class EmployeeView extends JPanel {

	private JTextField txtLastname;
	private JTextField txtFirstname;
	private JTextField txtLogin;
	private JComboBox<Store> comboBoxStore;
	private JComboBox<Role> comboBoxResponsability;

	private MainFrame mainFrame;
	private EmployeeController employeeController;
	private User selectedUser;
	private ArrayList<Store> stores;

	/**
	 * Create the panel.
	 * 
	 * @param mainFrame
	 * @param employeeController
	 */
	public EmployeeView(MainFrame mainFrame, EmployeeController employeeController) {
		this.employeeController = employeeController;
		this.mainFrame = mainFrame;
		stores = new ArrayList<Store>();
		buildView();
	}

	/**
	 * Build the view
	 */
	public void buildView() {
		setLayout(new BorderLayout(0, 0));

		JPanel panelHead = new JPanel();
		add(panelHead, BorderLayout.NORTH);
		panelHead.setLayout(new BorderLayout(0, 0));

		JPanel panelLeftHead = new JPanel();
		panelHead.add(panelLeftHead, BorderLayout.WEST);
		panelLeftHead.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		JButton btnReturn = new JButton("Retour");
		panelLeftHead.add(btnReturn);

		JButton btnUser = new JButton("Utilisateurs");
		btnUser.setEnabled(false);
		panelLeftHead.add(btnUser);

		JPanel panelRightHead = new JPanel();
		panelHead.add(panelRightHead, BorderLayout.EAST);
		panelRightHead.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		JButton btnDisconnect = new JButton("Se déconnecter");
		panelRightHead.add(btnDisconnect);

		JPanel panelTable = new JPanel();
		add(panelTable, BorderLayout.CENTER);
		panelTable.setLayout(new BorderLayout(0, 0));

		JPanel panelHeadTable = new JPanel();
		panelHeadTable.setBorder(new MatteBorder(2, 2, 2, 2, (Color) new Color(0, 0, 0)));
		panelTable.add(panelHeadTable, BorderLayout.CENTER);
		GridBagLayout gbl_panelHeadTable = new GridBagLayout();
		gbl_panelHeadTable.columnWidths = new int[] { 451, 0 };
		gbl_panelHeadTable.rowHeights = new int[] { 30, 30, 30, 30, 30, 30, 30, 30, 30, 0 };
		gbl_panelHeadTable.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_panelHeadTable.rowWeights = new double[] { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE };
		panelHeadTable.setLayout(gbl_panelHeadTable);

		JPanel panelTitleInformation = new JPanel();
		GridBagConstraints gbc_panelTitleInformation = new GridBagConstraints();
		gbc_panelTitleInformation.insets = new Insets(0, 0, 5, 0);
		gbc_panelTitleInformation.anchor = GridBagConstraints.NORTH;
		gbc_panelTitleInformation.gridx = 0;
		gbc_panelTitleInformation.gridy = 0;
		panelHeadTable.add(panelTitleInformation, gbc_panelTitleInformation);
		panelTitleInformation.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		JLabel lblEmployees = new JLabel("Employ\u00E9s");
		panelTitleInformation.add(lblEmployees);

		JPanel panelLastname = new JPanel();
		GridBagConstraints gbc_panelLastname = new GridBagConstraints();
		gbc_panelLastname.insets = new Insets(0, 0, 5, 0);
		gbc_panelLastname.fill = GridBagConstraints.BOTH;
		gbc_panelLastname.gridx = 0;
		gbc_panelLastname.gridy = 1;
		panelHeadTable.add(panelLastname, gbc_panelLastname);

		JLabel lblLastname = new JLabel("Nom : ");
		panelLastname.add(lblLastname);

		txtLastname = new JTextField();
		txtLastname.setText("Nom");
		panelLastname.add(txtLastname);
		txtLastname.setColumns(10);

		JPanel panelFirstname = new JPanel();
		GridBagConstraints gbc_panelFirstname = new GridBagConstraints();
		gbc_panelFirstname.insets = new Insets(0, 0, 5, 0);
		gbc_panelFirstname.fill = GridBagConstraints.BOTH;
		gbc_panelFirstname.gridx = 0;
		gbc_panelFirstname.gridy = 2;
		panelHeadTable.add(panelFirstname, gbc_panelFirstname);

		JLabel lblFirstname = new JLabel("Prenom :");
		panelFirstname.add(lblFirstname);

		txtFirstname = new JTextField();
		txtFirstname.setText("Prenom");
		panelFirstname.add(txtFirstname);
		txtFirstname.setColumns(10);

		JPanel panelResponsability = new JPanel();
		GridBagConstraints gbc_panelResponsability = new GridBagConstraints();
		gbc_panelResponsability.insets = new Insets(0, 0, 5, 0);
		gbc_panelResponsability.fill = GridBagConstraints.BOTH;
		gbc_panelResponsability.gridx = 0;
		gbc_panelResponsability.gridy = 3;
		panelHeadTable.add(panelResponsability, gbc_panelResponsability);

		JLabel lblResponsability = new JLabel("Responsabilit\u00E9 :");
		panelResponsability.add(lblResponsability);

		comboBoxResponsability = new JComboBox();
		panelResponsability.add(comboBoxResponsability);

		JPanel panelStroreAisle = new JPanel();
		GridBagConstraints gbc_panelStroreAisle = new GridBagConstraints();
		gbc_panelStroreAisle.insets = new Insets(0, 0, 5, 0);
		gbc_panelStroreAisle.fill = GridBagConstraints.BOTH;
		gbc_panelStroreAisle.gridx = 0;
		gbc_panelStroreAisle.gridy = 4;
		panelHeadTable.add(panelStroreAisle, gbc_panelStroreAisle);

		JLabel lblStoreAisle = new JLabel("Magasin :");
		panelStroreAisle.add(lblStoreAisle);

		comboBoxStore = new JComboBox();
		panelStroreAisle.add(comboBoxStore);

		JPanel panelLogin = new JPanel();
		GridBagConstraints gbc_panelLogin = new GridBagConstraints();
		gbc_panelLogin.insets = new Insets(0, 0, 5, 0);
		gbc_panelLogin.fill = GridBagConstraints.BOTH;
		gbc_panelLogin.gridx = 0;
		gbc_panelLogin.gridy = 5;
		panelHeadTable.add(panelLogin, gbc_panelLogin);

		JLabel lblLogin = new JLabel("Identifiant :");
		panelLogin.add(lblLogin);

		txtLogin = new JTextField();
		txtLogin.setText("Indentifiant");
		panelLogin.add(txtLogin);
		txtLogin.setColumns(10);

		JPanel panelBtnMenu = new JPanel();
		panelBtnMenu.setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));
		GridBagConstraints gbc_panelBtnMenu = new GridBagConstraints();
		gbc_panelBtnMenu.fill = GridBagConstraints.BOTH;
		gbc_panelBtnMenu.gridx = 0;
		gbc_panelBtnMenu.gridy = 7;
		panelHeadTable.add(panelBtnMenu, gbc_panelBtnMenu);

		JButton btnCancel = new JButton("Annuler");
		panelBtnMenu.add(btnCancel);

		JButton btnAdd = new JButton("Ajouter");
		panelBtnMenu.add(btnAdd);

		JButton btnChangePassWord = new JButton("R\u00E9initialiser mot de passe");
		panelBtnMenu.add(btnChangePassWord);

		JButton btnDelete = new JButton("Supprimer");
		panelBtnMenu.add(btnDelete);

		/**
		 * Setup listeners
		 */
		btnReturn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if(comboBoxStore.getItemCount() > 1)
					mainFrame.displayEmployeesView(null);// TODO
				else 
					mainFrame.displayEmployeesView(comboBoxStore.getItemAt(0));

				setVisible(false);
				mainFrame.displayEmployeesView(null);

			}
		});

		btnDisconnect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mainFrame.disconect();
			}
		});

		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if(comboBoxStore.getItemCount() > 1)
					mainFrame.displayEmployeesView(null);
				else 
					mainFrame.displayEmployeesView(comboBoxStore.getItemAt(0));

				setVisible(false);
				mainFrame.displayEmployeesView(null);
			}
		});

		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String passWord = employeeController.createUser(txtFirstname.getText(), txtLastname.getText(), txtLogin.getText(),(Store) comboBoxStore.getSelectedItem(), (Role)comboBoxResponsability.getSelectedItem());
				JOptionPane.showMessageDialog(null, "le nouveaux mot de passe est : " + passWord, "Erreur", JOptionPane.INFORMATION_MESSAGE);
						
				if(comboBoxStore.getItemCount() > 1)
					mainFrame.displayEmployeesView(null);
				else 
					mainFrame.displayEmployeesView(comboBoxStore.getItemAt(0));
			}
		});

		btnChangePassWord.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String password = employeeController.resetPassword(selectedUser);
				if (selectedUser != null) {
					JOptionPane.showMessageDialog(null, "Le nouveaux mot de passe est : " + password, "Erreur", JOptionPane.INFORMATION_MESSAGE);
				}
			}
		});

		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				employeeController.deleteEmployee(selectedUser);
				if(comboBoxStore.getItemCount() > 1)
					mainFrame.displayEmployeesView(null);
				else 
					mainFrame.displayEmployeesView(comboBoxStore.getItemAt(0));
			}
		});
	}

	/**
	 * Refresh the view
	 * 
	 * @param user
	 */
	public void refresh(User user) {
		comboBoxResponsability.removeAllItems();
		for (Role role : employeeController.getResponsabilities())
			comboBoxResponsability.addItem(role);
		
		comboBoxStore.removeAllItems();
		stores = employeeController.getStores();
		for (Store store : stores) {
			comboBoxStore.addItem(store);
		}
		if (user == null) {
			txtLastname.setText("");
			txtFirstname.setText("");
			txtLogin.setText("");
			
		} else {
			selectedUser = user;
			
			
		}

	}

}
