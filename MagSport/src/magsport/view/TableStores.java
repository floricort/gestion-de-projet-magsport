package magsport.view;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import magsport.model.Store;
import magsport.model.User;

public class TableStores extends AbstractTableModel {

	final String[] columnsTitle = { "Nom", "Ville", "Address", "Responssable" };
	private ArrayList<Store> listStores;
	private ArrayList<ArrayList<Object>> tableValues;

	/**
	 * Constructor
	 */
	public TableStores(ArrayList<Store> listStores) {
		tableValues = new ArrayList<ArrayList<Object>>();
		if (listStores == null)
			this.listStores = new ArrayList<Store>();
		else
			this.listStores = listStores;
		refresh();
	}

	/**
	 * Return the number of columns
	 * 
	 * @return the number of columns
	 */
	@Override
	public int getColumnCount() {
		return columnsTitle.length;
	}

	/**
	 * Return the number of rows
	 * 
	 * @return the number of rows
	 */
	public int getRowCounter() {
		return tableValues.size();
	}

	/**
	 * Return the class type of the column whose the index is pass in parameter
	 * 
	 * @param columnIndex the index of the column
	 * @return the class of the column
	 */
	@Override
	public Class getColumnClass(int columnIndex) {
		if (columnIndex >= getColumnCount()) {
			throw new IndexOutOfBoundsException();
		}
		// return tableValues.get(0).get(columnIndex).getClass();
		switch (columnIndex) {
		case 0:
		case 1:
		case 2:
			return String.class;
		case 3:
			return User.class;
		default:
			return String.class;
		}

	}

	/**
	 * Return the name of the column pass in parameter
	 * 
	 * @param columnIndex the index
	 * @return the name of the column
	 */
	@Override
	public String getColumnName(int columnIndex) throws IndexOutOfBoundsException {
		if (columnIndex >= getColumnCount()) {
			throw new IndexOutOfBoundsException();
		}
		return columnsTitle[columnIndex];
	}

	/**
	 * Return the number of rows in table
	 * 
	 * @return the number of rows
	 */
	@Override
	public int getRowCount() {
		return tableValues.size();
	}

	/**
	 * Return the value at the position pass in parameter
	 * 
	 * @param rowIndex    row index of table
	 * @param columnIndex row index of table
	 */
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) throws IndexOutOfBoundsException {
		if (rowIndex >= getRowCount() || columnIndex >= getColumnCount()) {
			throw new IndexOutOfBoundsException();
		}
		return tableValues.get(rowIndex).get(columnIndex);
	}

	/**
	 * Return if the cell is editable
	 * 
	 * @param rowIndex    row index of table
	 * @param columnIndex row index of table
	 * @return if the cell is editable
	 */
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) throws IndexOutOfBoundsException {
		if (rowIndex >= getRowCount() || columnIndex >= getColumnCount()) {
			throw new IndexOutOfBoundsException();
		}
		return false;
	}

	/**
	 * set listLocal
	 */
	public void setList(ArrayList<Store> list) {
		this.listStores.clear();
		this.listStores.addAll(list);
		refresh();
	}

	public void addToList(Store store) {
		listStores.add(store);
		refresh();
	}

	public void suprToList(Store store) {
		listStores.remove(store);
		refresh();
	}

	/**
	 * Refresh the table
	 */
	public void refresh() {
		tableValues.clear();
		for (Store store : listStores) {
			ArrayList<Object> newRow = new ArrayList<Object>();
			newRow.add(store.getName());
			newRow.add(store.getCity());
			newRow.add(store.getAddress());
			newRow.add(store.getPersonInCharge());
			tableValues.add(newRow);
		}
		fireTableDataChanged();
	}

}
