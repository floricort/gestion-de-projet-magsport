package magsport.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import magsport.controller.AiselAddEditController;
import magsport.controller.AisleController;
import magsport.controller.EmployeeController;
import magsport.controller.EmployeesController;
import magsport.controller.LoginController;
import magsport.controller.MagSportController;
import magsport.controller.ProductAddEditController;
import magsport.controller.StoreAddEditController;
import magsport.controller.StoreController;
import magsport.controller.StoresController;
import magsport.model.Department;
import magsport.model.Product;
import magsport.model.Store;
import magsport.model.User;

import java.awt.CardLayout;

public class MainFrame extends JFrame {
	
	private StoresView storesView;
	private StoreView storeView;
	private StoreAddEditView storeAddEditView;
	private AisleView aisleView;
	private AiselAddEditView aiselAddEditView;
	private EmployeesView employeesView;
	private EmployeeView employeeView;
	private ProductAddEditView productAddEditView;
	private LoginView loginView;
	private MagSportController magSportController;
	
	private JPanel contentPane;

	/**
	 * Create the frame.
	 */
	public MainFrame(MagSportController magSportController) {
		this.magSportController = magSportController;
		buildFrame();
		
	}
	
	private void buildFrame() {
	
		storesView = new StoresView(this, new StoresController(magSportController));
		storeView = new StoreView(this, new StoreController(magSportController));
		storeAddEditView = new StoreAddEditView(this,new StoreAddEditController(magSportController));
		aisleView = new AisleView(this, new AisleController(magSportController));
		aiselAddEditView = new AiselAddEditView(this, new AiselAddEditController(magSportController));
		employeesView = new EmployeesView(this, new EmployeesController(magSportController));
		employeeView = new EmployeeView(this, new EmployeeController(magSportController));
		productAddEditView = new ProductAddEditView(this, new ProductAddEditController(magSportController));
		loginView = new LoginView(this, new LoginController(magSportController));
		contentPane = new JPanel();
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1093, 692);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new CardLayout(0, 0));
		
		contentPane.add(storesView);
		contentPane.add(storeView);
		contentPane.add(storeAddEditView);
		contentPane.add(aisleView);
		contentPane.add(aiselAddEditView);
		contentPane.add(employeesView);
		contentPane.add(employeeView);
		contentPane.add(productAddEditView);
		contentPane.add(loginView);
		
		
		
		storesView.setVisible(false);
		storeView.setVisible(false);
		storeAddEditView.setVisible(false);
		aisleView.setVisible(false);
		aiselAddEditView.setVisible(false);
		employeesView.setVisible(false);
		employeeView.setVisible(false);
		productAddEditView.setVisible(false);
		loginView.setVisible(true);
		this.setVisible(true);
		//loginView.
	}

	public void displayUserFirstView() {
		switch (magSportController.getLoggedInUser().getRole()) {
		case ROLE_CEO:
			loginView.setVisible(false);
			storeView.setVisible(false);
			storeAddEditView.setVisible(false);
			aisleView.setVisible(false);
			aiselAddEditView.setVisible(false);
			employeesView.setVisible(false);
			employeeView.setVisible(false);
			productAddEditView.setVisible(false);
			storesView.setVisible(true);
			storesView.refresh();
			break;
		case ROLE_STORE_MANAGER:
			loginView.setVisible(false);
			storeView.setVisible(true);
			storeView.refresh(magSportController.getStoreOfLoggedUser());
			break;
		case ROLE_DEPARTMENT_MANAGER:
			loginView.setVisible(false);
			aisleView.setVisible(true);
			aisleView.refresh(magSportController.getAiselOfLoggedUser(), magSportController.getStoreOfLoggedUser());
			//TODO 
			break;
		default:
			break;
		}	
	}
	
	public void displayStoresView() {
		switch (magSportController.getLoggedInUser().getRole()) {
		case ROLE_CEO:
			storeView.setVisible(false);
			storeAddEditView.setVisible(false);
			employeesView.setVisible(false);
			storesView.setVisible(true);
			storesView.refresh();
			break;
		case ROLE_STORE_MANAGER:
		case ROLE_DEPARTMENT_MANAGER:
		default:
			break;
		}	
	}
	
	public void displayStoreView(Store store) {
		switch (magSportController.getLoggedInUser().getRole()) {
		case ROLE_CEO:
		case ROLE_STORE_MANAGER:
			storesView.setVisible(false);
			storeAddEditView.setVisible(false);
			employeesView.setVisible(false);
			aiselAddEditView.setVisible(false);
			aisleView.setVisible(false);
			storeView.setVisible(true);
			storeView.refresh(store);
			break;
		case ROLE_DEPARTMENT_MANAGER:
		default:
			break;
		}	
	}
	
	public void displayStoreAddEditView(Store store){
		switch (magSportController.getLoggedInUser().getRole()) {
		case ROLE_CEO:
			storeView.setVisible(false);
			storesView.setVisible(false);
			storeAddEditView.setVisible(true);
			storeAddEditView.refresh(store);
			break;
		case ROLE_STORE_MANAGER:
		case ROLE_DEPARTMENT_MANAGER:
		default:
			break;
		}
	}
	
	public void displayAisleView(Department department, Store store){
		switch (magSportController.getLoggedInUser().getRole()) {
		case ROLE_CEO:
		case ROLE_STORE_MANAGER:
		case ROLE_DEPARTMENT_MANAGER:
			storeView.setVisible(false);
			aiselAddEditView.setVisible(false);
			productAddEditView.setVisible(false);
			aisleView.setVisible(true);

			aisleView.refresh(department, store);
			break;
		default:
			break;
		}
	}
	
	public void displayAiselAddEditView(Department department, Store store){
		switch (magSportController.getLoggedInUser().getRole()) {
		case ROLE_CEO:
		case ROLE_STORE_MANAGER:
			storeView.setVisible(false);
			aisleView.setVisible(false);
			aiselAddEditView.setVisible(true);

			aiselAddEditView.refresh(department, store);
			break;
		case ROLE_DEPARTMENT_MANAGER:
		default:
			break;
		}
	}

	public void displayProductAddEditView(Product product, Department department, Store store){
		switch (magSportController.getLoggedInUser().getRole()) {
		case ROLE_CEO:
		case ROLE_STORE_MANAGER:
		case ROLE_DEPARTMENT_MANAGER:
			aisleView.setVisible(false);
			
			productAddEditView.setVisible(true);
			productAddEditView.refresh(product, department,store);
			break;
		default:
			break;
		}
	}
	
	public void displayEmployeesView(Store store){
		switch (magSportController.getLoggedInUser().getRole()) {
		case ROLE_CEO:
		case ROLE_STORE_MANAGER:
			storesView.setVisible(false);
			storeView.setVisible(false);
			storeAddEditView.setVisible(false);
			aisleView.setVisible(false);
			aiselAddEditView.setVisible(false);
			employeeView.setVisible(false);
			productAddEditView.setVisible(false);
			employeesView.setVisible(true);
			
			employeesView.refresh(store);
			break;
		case ROLE_DEPARTMENT_MANAGER:
		default:
			break;
		}
	}
	
	public void displayEmployeeView(User user){
		switch (magSportController.getLoggedInUser().getRole()) {
		case ROLE_CEO:
		case ROLE_STORE_MANAGER:
			storesView.setVisible(false);
			storeView.setVisible(false);
			storeAddEditView.setVisible(false);
			aisleView.setVisible(false);
			aiselAddEditView.setVisible(false);
			employeesView.setVisible(false);
			productAddEditView.setVisible(false);
			employeeView.setVisible(true);
			
			employeeView.refresh(user);
			break;
		case ROLE_DEPARTMENT_MANAGER:
		default:
			break;
		}
	}
	
	/**
	 * Disconnect the logged user
	 */
	public void disconect() {
		storesView.setVisible(false);
		storeView.setVisible(false);
		storeAddEditView.setVisible(false);
		aisleView.setVisible(false);
		aiselAddEditView.setVisible(false);
		employeesView.setVisible(false);
		employeeView.setVisible(false);
		productAddEditView.setVisible(false);
		
		magSportController.logout();
		loginView.setVisible(true);
		loginView.refresh();
	}
}
