package magsport.view;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import magsport.controller.LoginController;

import javax.swing.JLabel;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.FlowLayout;
import javax.swing.JTextField;
import java.awt.Insets;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;



public class LoginView extends JPanel {

	private LoginController loginController;
	private MainFrame mainFrame;
	private JTextField txtLogin;
	private JTextField txtPassWord;

	
	/**
	 * Create the frame.
	 * @param mainFrame 
	 */
	public LoginView(MainFrame mainFrame, LoginController loginController) {
		this.mainFrame = mainFrame;
		this.loginController = loginController;
		buildView();
	}
	
	
	/**
	 * Build the view
	 */
	private void buildView() {
		/**
		 * Create components
		 */
		JPanel panelHead = new JPanel();
		JPanel panelLoginIn = new JPanel();
		JPanel panelPassWord = new JPanel();
		JPanel panelBtmConnection = new JPanel();
		JPanel panel_4 = new JPanel();
		
		GridBagLayout gbl_panelLoginIn = new GridBagLayout();
		GridBagConstraints gbc_panelBtmConnection = new GridBagConstraints();
		GridBagConstraints gbc_panel_4 = new GridBagConstraints();
		GridBagConstraints gbc_panelPassWord = new GridBagConstraints();
		
		JLabel lblConnection = new JLabel("Connexion");
		JLabel lblLogin = new JLabel("Identifiant");
		JLabel lblPassWord = new JLabel("Mot de passe");
		JButton btnConnection = new JButton("Connexion");
		txtLogin = new JTextField();
		txtPassWord = new JTextField();
		
		/**
		 * Setup components
		 */
		setBounds(100, 100, 402, 264);

		
		gbl_panelLoginIn.columnWidths = new int[] {0, 0};
		gbl_panelLoginIn.rowHeights = new int[] {30, 30, 30, 30};
		gbl_panelLoginIn.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_panelLoginIn.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0};
		panelLoginIn.setLayout(gbl_panelLoginIn);
		
		panel_4.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		gbc_panelBtmConnection.insets = new Insets(0, 0, 5, 0);
		gbc_panelBtmConnection.fill = GridBagConstraints.BOTH;
		gbc_panelBtmConnection.gridx = 0;
		gbc_panelBtmConnection.gridy = 0;
		
		gbc_panelPassWord.insets = new Insets(0, 0, 5, 0);
		gbc_panelPassWord.fill = GridBagConstraints.BOTH;
		gbc_panelPassWord.gridx = 0;
		gbc_panelPassWord.gridy = 1;
		
		gbc_panel_4.fill = GridBagConstraints.BOTH;
		gbc_panel_4.gridx = 0;
		gbc_panel_4.gridy = 2;
		
		panelBtmConnection.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		txtLogin.setColumns(10);
		txtPassWord.setColumns(10);
		
		/**
		 * Link components
		 */
		setLayout(new BorderLayout(0, 0));
		
		add(panelHead, BorderLayout.NORTH);
		add(panelLoginIn);
		
		panelHead.add(lblConnection);
		
		panelLoginIn.add(panelPassWord, gbc_panelPassWord);
		panelLoginIn.add(panelBtmConnection, gbc_panelBtmConnection);
		panelLoginIn.add(panel_4, gbc_panel_4);
		
		panel_4.add(btnConnection);
		
		panelPassWord.add(lblPassWord);
		panelPassWord.add(txtPassWord);
		
		panelBtmConnection.add(lblLogin);
		panelBtmConnection.add(txtLogin);

		
		/**
		 * Setup listeners
		 */
		btnConnection.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				connectionBtnPressed();
			}
		});

	}
	
	
	/**
	 * Method call by a pressure on connection button
	 */
	private void connectionBtnPressed() {
		if (loginController.connection(txtLogin.getText(), txtPassWord.getText()))
			mainFrame.displayUserFirstView();
	}


	public void refresh() {
		txtLogin.setText("");
		txtPassWord.setText("");
	}
}
