package magsport.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;

import magsport.controller.StoreController;
import magsport.model.Department;
import magsport.model.Role;
import magsport.model.Store;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class StoreView extends JPanel {

	private StoreController storeController;
	private MainFrame mainFrame;
	private JTable tableAisel;
	private Store currentStore;

	private JLabel lblNomrespossable;
	private JLabel lblAddress;
	private JLabel lblNomMagasin;
	private TableAisel AbstractTableAisel;
	private JButton btnModifierStore;

	/**
	 * Create the panel.
	 * 
	 * @param mainFrame
	 */
	public StoreView(MainFrame mainFrame, StoreController storeController) {
		this.storeController = storeController;
		this.mainFrame = mainFrame;
		buildView();
	}

	private void buildView() {
		/**
		 * Create components
		 */

		JPanel panelCoreTableMagasin = new JPanel();
		JPanel panelHead = new JPanel();
		JPanel panelLeftHead = new JPanel();
		JPanel panelRightHead = new JPanel();
		JPanel panelTableMagasin = new JPanel();
		JPanel panelHeadTableMagasin = new JPanel();
		JPanel panelTitleInformation = new JPanel();
		JPanel panelTitle = new JPanel();
		JPanel panelInformation = new JPanel();
		JPanel panelBtn = new JPanel();
		JPanel panelBtnAddRayon = new JPanel();
		JPanel panelBtnModifierMag = new JPanel();

		FlowLayout fl_panelBtnAddRayon = (FlowLayout) panelBtnAddRayon.getLayout();

		JLabel lblStore = new JLabel("Magasin");
		JLabel lblNom = new JLabel("Nom : ");
		lblNomMagasin = new JLabel("MonMagasin");
		JLabel lblImplentation = new JLabel("|  Implentation : ");
		lblAddress = new JLabel("Address");
		JLabel lblResponsable = new JLabel("|  Responsable : ");
		lblNomrespossable = new JLabel("NomRespossable");

		JButton btnAddAisle = new JButton("Ajouter un rayon");
		btnModifierStore = new JButton("Modifier Magasin");
		JButton btnSeDconecter = new JButton("Se d\u00E9conecter");
		JButton btnReturnStores = new JButton("Retour Liste Magasin");
		JButton btnGoEmployees = new JButton("Utilisateurs");

		JScrollPane scrollPaneMagasin = new JScrollPane();

		AbstractTableAisel = new TableAisel(null);
		tableAisel = new JTable(AbstractTableAisel);

		/**
		 * Setup components
		 */
		panelCoreTableMagasin.setLayout(new BorderLayout(0, 0));
		scrollPaneMagasin.setViewportView(tableAisel);
		panelHead.setLayout(new BorderLayout(0, 0));
		panelLeftHead.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		panelRightHead.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		panelTableMagasin.setLayout(new BorderLayout(0, 0));
		panelHeadTableMagasin.setLayout(new BorderLayout(0, 0));
		panelTitleInformation.setLayout(new BorderLayout(0, 0));
		panelTitle.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		panelInformation.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
		panelBtn.setLayout(new BorderLayout(0, 0));
		fl_panelBtnAddRayon.setAlignment(FlowLayout.RIGHT);
		btnAddAisle.setHorizontalAlignment(SwingConstants.RIGHT);

		/**
		 * Link components
		 */
		setLayout(new BorderLayout(0, 0));

		add(panelHead, BorderLayout.NORTH);
		add(panelTableMagasin, BorderLayout.CENTER);

		panelHead.add(panelLeftHead, BorderLayout.WEST);
		panelHead.add(panelRightHead, BorderLayout.EAST);

		panelBtnModifierMag.add(btnModifierStore);

		panelTableMagasin.add(panelCoreTableMagasin, BorderLayout.CENTER);
		panelTableMagasin.add(panelHeadTableMagasin, BorderLayout.NORTH);

		panelCoreTableMagasin.add(scrollPaneMagasin);

		panelLeftHead.add(btnReturnStores);
		panelLeftHead.add(btnGoEmployees);

		panelRightHead.add(btnSeDconecter);

		panelHeadTableMagasin.add(panelTitleInformation, BorderLayout.CENTER);
		panelHeadTableMagasin.add(panelBtn, BorderLayout.SOUTH);

		panelTitle.add(lblStore);

		panelTitleInformation.add(panelTitle, BorderLayout.NORTH);
		panelTitleInformation.add(panelInformation, BorderLayout.SOUTH);

		panelInformation.add(lblNom);
		panelInformation.add(lblNomMagasin);
		panelInformation.add(lblImplentation);
		panelInformation.add(lblAddress);
		panelInformation.add(lblResponsable);
		panelInformation.add(lblNomrespossable);

		panelBtn.add(panelBtnAddRayon, BorderLayout.EAST);
		panelBtn.add(panelBtnModifierMag, BorderLayout.WEST);

		panelBtnAddRayon.add(btnAddAisle);

		/**
		 * Setup listeners
		 */
		btnReturnStores.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mainFrame.displayStoresView();
			}
		});
		btnGoEmployees.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mainFrame.displayEmployeesView(currentStore);
			}
		});
		btnSeDconecter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mainFrame.disconect();
			}
		});
		btnAddAisle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mainFrame.displayAiselAddEditView(null, currentStore);
			}
		});
		btnModifierStore.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mainFrame.displayStoreAddEditView(currentStore);
			}
		});
		tableAisel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {
					Integer row = tableAisel.getSelectedRow();
					String name = (String) tableAisel.getValueAt(row, 0);
					Department selectedDepartment = storeController.getAisel(name, currentStore);
					if (selectedDepartment == null)
						JOptionPane.showMessageDialog(null, "ce rayon n'existe pas", "Erreur",
								JOptionPane.ERROR_MESSAGE);
					else
						mainFrame.displayAisleView(selectedDepartment, currentStore);
				}
			}
		});
	}

	public void refresh(Store store) {
		currentStore = store;
		lblAddress.setText(store.getAddress() + "," + store.getCity());
		lblNomMagasin.setText(store.getName());
		if (store.getPersonInCharge() != null)
			lblNomrespossable.setText(store.getPersonInCharge().toString());

		AbstractTableAisel.setList(store.getDepartments());
		// TODO Auto-generated method stub
		if (storeController.GetUserPermission(store))
			btnModifierStore.setEnabled(true);
		else
			btnModifierStore.setEnabled(false);

	}

}
