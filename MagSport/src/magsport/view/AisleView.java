package magsport.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;

import magsport.controller.AisleController;
import magsport.controller.ProductAddEditController;
import magsport.model.Department;
import magsport.model.Product;
import magsport.model.Store;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AisleView extends JPanel {
	private MainFrame mainFrame;
	private AisleController aisleController;
	private JTable tableProduct;
	private TableProducts abstractTableProducts;
	private JLabel lblAisleName;
	private JLabel lblManagerName;
	private Department department;
	private Store store;

	/**
	 * Create the panel.
	 * @param mainFrame 
	 * @param aisleController 
	 */
	public AisleView(MainFrame mainFrame, AisleController aisleController) {
		this.aisleController = aisleController;
		this.mainFrame = mainFrame;
		setLayout(new BorderLayout(0, 0));
		
		JPanel panelHead = new JPanel();
		add(panelHead, BorderLayout.NORTH);
		panelHead.setLayout(new BorderLayout(0, 0));
		
		JPanel panelLeftHead = new JPanel();
		panelHead.add(panelLeftHead, BorderLayout.WEST);
		panelLeftHead.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JButton btnReturnStore = new JButton("Retour Magasin");
		btnReturnStore.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mainFrame.displayStoreView(store);
			}
		});
		panelLeftHead.add(btnReturnStore);
		
		JButton btnUsers = new JButton("Utilisateurs");
		btnUsers.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mainFrame.displayEmployeesView(store);
			}
		});
		panelLeftHead.add(btnUsers);
		
		JPanel panelRightHead = new JPanel();
		panelHead.add(panelRightHead, BorderLayout.EAST);
		panelRightHead.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JButton btnSeDconecter = new JButton("Se d\u00E9conecter");
		btnSeDconecter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mainFrame.disconect();
			}
		});
		panelRightHead.add(btnSeDconecter);
		
		JPanel panelTableMagasin = new JPanel();
		add(panelTableMagasin, BorderLayout.CENTER);
		panelTableMagasin.setLayout(new BorderLayout(0, 0));
		
		JPanel panelHeadTableMagasin = new JPanel();
		panelTableMagasin.add(panelHeadTableMagasin, BorderLayout.NORTH);
		panelHeadTableMagasin.setLayout(new BorderLayout(0, 0));
		
		JPanel panelTitleInformation = new JPanel();
		panelHeadTableMagasin.add(panelTitleInformation, BorderLayout.CENTER);
		panelTitleInformation.setLayout(new BorderLayout(0, 0));
		
		JPanel panelTitle = new JPanel();
		panelTitleInformation.add(panelTitle, BorderLayout.NORTH);
		panelTitle.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JLabel lblAisle = new JLabel("Rayon");
		panelTitle.add(lblAisle);
		
		JPanel panelInformation = new JPanel();
		panelTitleInformation.add(panelInformation, BorderLayout.SOUTH);
		panelInformation.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
		
		JLabel lblName = new JLabel("Nom : ");
		panelInformation.add(lblName);
		
		lblAisleName = new JLabel("MonRayon");
		panelInformation.add(lblAisleName);
		
		JLabel lblManager = new JLabel("|  Responsable : ");
		panelInformation.add(lblManager);

		lblManagerName = new JLabel("NomRespossable");
		panelInformation.add(lblManagerName);
		
		JPanel panelBtn = new JPanel();
		panelHeadTableMagasin.add(panelBtn, BorderLayout.SOUTH);
		panelBtn.setLayout(new BorderLayout(0, 0));
		
		JPanel panelBtnAddProduct = new JPanel();
		FlowLayout fl_panelBtnAddProduct = (FlowLayout) panelBtnAddProduct.getLayout();
		fl_panelBtnAddProduct.setAlignment(FlowLayout.RIGHT);
		panelBtn.add(panelBtnAddProduct, BorderLayout.EAST);
		
		JButton btnAddProduct = new JButton("Ajouter un produit");
		btnAddProduct.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mainFrame.displayProductAddEditView(null,department,store);

			}
		});
		panelBtnAddProduct.add(btnAddProduct);
		btnAddProduct.setHorizontalAlignment(SwingConstants.RIGHT);
		
		JPanel panelBtnModifierMag = new JPanel();
		panelBtn.add(panelBtnModifierMag, BorderLayout.WEST);
		
		JButton btnModifierAisle = new JButton("Modifier Rayon");
		btnModifierAisle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mainFrame.displayAiselAddEditView(department, store);

			}
		});
		panelBtnModifierMag.add(btnModifierAisle);
		
		JPanel panelCoreTableMagasin = new JPanel();
		panelTableMagasin.add(panelCoreTableMagasin, BorderLayout.CENTER);
		panelCoreTableMagasin.setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPaneMagasin = new JScrollPane();
		panelCoreTableMagasin.add(scrollPaneMagasin);

		abstractTableProducts = new TableProducts(null);
		tableProduct = new JTable(abstractTableProducts);
		scrollPaneMagasin.setViewportView(tableProduct);
		
	}

	public void refresh(Department department, Store store) {
		// TODO Auto-generated method stub
		this.department = department;
		this.store = store;
		abstractTableProducts.setList(department.getProducts());
		lblAisleName.setText(department.getName());
		if (department.getPersonInCharge() != null)
			lblManagerName.setText(department.getPersonInCharge().toString());
	}

}
