package magsport.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;

import magsport.controller.EmployeesController;
import magsport.model.Store;
import magsport.model.User;

public class EmployeesView extends JPanel {
	
	private MainFrame mainFrame;
	private JTable tableEmployees;
	private TableEmployees abstractTableEmployees;
	private EmployeesController employeesController;
	private Store currentStore;//null if they have all employees (user)s

	/**
	 * Create the panel.
	 * @param mainFrame 
	 * @param employeesController 
	 */
	public EmployeesView(MainFrame mainFrame, EmployeesController employeesController) {
		this.employeesController = employeesController;
		this.mainFrame = mainFrame;
		
		buildView();
	}
	
	/**
	 * Build the view
	 */
	private void buildView() {
		setLayout(new BorderLayout(0, 0));

		JPanel panelHead = new JPanel();
		add(panelHead, BorderLayout.NORTH);
		panelHead.setLayout(new BorderLayout(0, 0));

		JPanel panelLeftHead = new JPanel();
		panelHead.add(panelLeftHead, BorderLayout.WEST);
		panelLeftHead.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		JButton btnReturn = new JButton("Retour");
		
		panelLeftHead.add(btnReturn);

		JButton btnUser = new JButton("Utilisateurs");
		btnUser.setEnabled(false);
		
		panelLeftHead.add(btnUser);

		JPanel panelRightHead = new JPanel();
		panelHead.add(panelRightHead, BorderLayout.EAST);
		panelRightHead.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		JButton btnDisconnect = new JButton("Se déconnecter");
		
		panelRightHead.add(btnDisconnect);

		JPanel panelTable = new JPanel();
		add(panelTable, BorderLayout.CENTER);
		panelTable.setLayout(new BorderLayout(0, 0));

		JPanel panelHeadTable = new JPanel();
		panelTable.add(panelHeadTable, BorderLayout.NORTH);
		panelHeadTable.setLayout(new BorderLayout(0, 0));

		JPanel panelTitleInformation = new JPanel();
		panelHeadTable.add(panelTitleInformation, BorderLayout.CENTER);
		panelTitleInformation.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		JLabel lblEmployees = new JLabel("Employ\u00E9s");
		panelTitleInformation.add(lblEmployees);

		JPanel panelBtn = new JPanel();
		panelHeadTable.add(panelBtn, BorderLayout.SOUTH);
		panelBtn.setLayout(new BorderLayout(0, 0));

		JPanel panelBtnAddEmployee = new JPanel();
		FlowLayout fl_panelBtnAddEmployee = (FlowLayout) panelBtnAddEmployee.getLayout();
		fl_panelBtnAddEmployee.setAlignment(FlowLayout.RIGHT);
		panelBtn.add(panelBtnAddEmployee, BorderLayout.EAST);

		JButton btnAddEmployee = new JButton("Ajouter un employer");
		
		panelBtnAddEmployee.add(btnAddEmployee);
		btnAddEmployee.setHorizontalAlignment(SwingConstants.RIGHT);

		JPanel panelCoreTable = new JPanel();
		panelTable.add(panelCoreTable, BorderLayout.CENTER);
		panelCoreTable.setLayout(new BorderLayout(0, 0));

		JScrollPane scrollPaneEmployees = new JScrollPane();
		panelCoreTable.add(scrollPaneEmployees);

		abstractTableEmployees = new TableEmployees(null, employeesController);
		tableEmployees = new JTable(abstractTableEmployees);
		scrollPaneEmployees.setViewportView(tableEmployees);
		
		/**
		 * Setup listeners
		 */
		btnReturn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mainFrame.displayUserFirstView();
			}
		});
		
		btnDisconnect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mainFrame.disconect();
			}
		});
		
		btnAddEmployee.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mainFrame.displayEmployeeView(null);
			}
		});
		
		// double click on a employee in table
		tableEmployees.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {
					JTable target = (JTable)e.getSource();
					Integer row = target.getSelectedRow();
					User user = abstractTableEmployees.getUserByIndex(row);
					mainFrame.displayEmployeeView(user);
				}
			}
		});
	}

	/**
	 * Refresh the view
	 * @param store 
	 */
	public void refresh(Store store) {
		ArrayList<User> employeesList = employeesController.getEmployeesList();
		abstractTableEmployees.setList(employeesList);
	}

}
