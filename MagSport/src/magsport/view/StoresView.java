package magsport.view;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;

import magsport.controller.StoresController;
import magsport.model.Store;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class StoresView extends JPanel {

	private StoresController storesController;
	private MainFrame mainFrame;
	private JTable tableMagasin;
	private TableStores abstractTableStore;

	/**
	 * Create the panel.
	 * 
	 * @param mainFrame
	 */
	public StoresView(MainFrame mainFrame, StoresController storesController) {
		this.storesController = storesController;
		this.mainFrame = mainFrame;
		buildView();
	}

	/**
	 * Build the view
	 */
	private void buildView() {

		/**
		 * Create components
		 */
		JPanel panelHead = new JPanel();
		JPanel panelLeftHead = new JPanel();
		JPanel panelTitle = new JPanel();
		JPanel panelBtnAdd = new JPanel();
		JPanel panelRightHead = new JPanel();
		JPanel panelTableMagasin = new JPanel();
		JPanel panelCoreTableMagasin = new JPanel();
		JPanel panelHeadTableMagasin = new JPanel();

		JLabel lblStore = new JLabel("Magasins");
		JButton btnReturnToStores = new JButton("Magasin");
		JButton btnEmployees = new JButton("Utilisateur");
		JButton btnDisconnect = new JButton("Se d\u00E9conecter");
		JButton btnAddStore = new JButton("Ajouter un magasin");
		JScrollPane scrollPaneMagasin = new JScrollPane();

		abstractTableStore = new TableStores(null);
		tableMagasin = new JTable(abstractTableStore);

		/**
		 * Setup components
		 */
		setLayout(new BorderLayout(0, 0));
		panelHead.setLayout(new BorderLayout(0, 0));
		panelLeftHead.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		btnReturnToStores.setEnabled(false);
		panelRightHead.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		panelTableMagasin.setLayout(new BorderLayout(0, 0));
		panelHeadTableMagasin.setLayout(new BorderLayout(0, 0));
		panelTitle.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		panelBtnAdd.setLayout(new FlowLayout(FlowLayout.RIGHT, 5, 5));
		btnAddStore.setHorizontalAlignment(SwingConstants.RIGHT);
		panelCoreTableMagasin.setLayout(new BorderLayout(0, 0));
		scrollPaneMagasin.setViewportView(tableMagasin);

		/**
		 * Link components
		 */
		add(panelHead, BorderLayout.NORTH);
		add(panelTableMagasin, BorderLayout.CENTER);

		panelHead.add(panelLeftHead, BorderLayout.WEST);
		panelHead.add(panelRightHead, BorderLayout.EAST);

		panelLeftHead.add(btnReturnToStores);
		panelLeftHead.add(btnEmployees);

		panelRightHead.add(btnDisconnect);

		panelTableMagasin.add(panelHeadTableMagasin, BorderLayout.NORTH);
		panelTableMagasin.add(panelCoreTableMagasin, BorderLayout.CENTER);

		panelHeadTableMagasin.add(panelTitle, BorderLayout.CENTER);
		panelHeadTableMagasin.add(panelBtnAdd, BorderLayout.SOUTH);

		panelTitle.add(lblStore);

		panelBtnAdd.add(btnAddStore);

		panelCoreTableMagasin.add(scrollPaneMagasin);

		/**
		 * Setup listeners
		 */
		btnEmployees.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mainFrame.displayEmployeesView(null);
			}
		});
		btnDisconnect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mainFrame.disconect();
			}
		});
		btnAddStore.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mainFrame.displayStoreAddEditView(null);
			}
		});
		tableMagasin.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {
					Integer row = tableMagasin.getSelectedRow();
					String name = (String) tableMagasin.getValueAt(row, 0);
					Store selectedStore = storesController.getStore(name);
					if (selectedStore == null)
						JOptionPane.showMessageDialog(null, "ce magasin n'existe pas", "Erreur",
								JOptionPane.ERROR_MESSAGE);
					else
						mainFrame.displayStoreView(selectedStore);
				}
			}
		});

		/**
		 * Set visibility
		 */

	}

	public void refresh() {
		abstractTableStore.setList(storesController.getListStore());
	}

}
