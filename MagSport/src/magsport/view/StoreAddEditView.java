package magsport.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.MatteBorder;

import magsport.controller.StoreAddEditController;
import magsport.model.Role;
import magsport.model.Store;
import magsport.model.User;

public class StoreAddEditView extends JPanel {

	private MainFrame mainFrame;
	private StoreAddEditController storeAddEditController;
	private Store store;
	private JTextField txtName;
	private JTextField txtCity;
	private JTextField txtAdresse;
	private JComboBox<User> comboBoxManager;
	private JButton btnAdd;

	/**
	 * Create the panel.
	 * 
	 * @param mainFrame
	 * @param storeAddEditController
	 */
	public StoreAddEditView(MainFrame mainFrame, StoreAddEditController storeAddEditController) {
		this.storeAddEditController = storeAddEditController;
		this.mainFrame = mainFrame;
		setLayout(new BorderLayout(0, 0));

		JPanel panelHeadTable = new JPanel();
		add(panelHeadTable, BorderLayout.CENTER);
		panelHeadTable.setBorder(new MatteBorder(2, 2, 2, 2, (Color) new Color(0, 0, 0)));
		GridBagLayout gbl_panelHeadTable = new GridBagLayout();
		gbl_panelHeadTable.rowHeights = new int[] { 0, 0, 0, 0, 0 };
		gbl_panelHeadTable.columnWidths = new int[] { 200, 200 };
		gbl_panelHeadTable.columnWeights = new double[] { 1.0, 1.0 };
		gbl_panelHeadTable.rowWeights = new double[] { 1.0, 1.0, 1.0, 1.0, 1.0 };
		panelHeadTable.setLayout(gbl_panelHeadTable);

		JLabel lblName = new JLabel("Nom : ");
		GridBagConstraints gbc_lblName = new GridBagConstraints();
		gbc_lblName.anchor = GridBagConstraints.EAST;
		gbc_lblName.insets = new Insets(0, 0, 5, 5);
		gbc_lblName.gridx = 0;
		gbc_lblName.gridy = 0;
		panelHeadTable.add(lblName, gbc_lblName);

		txtName = new JTextField();
		GridBagConstraints gbc_txtName = new GridBagConstraints();
		gbc_txtName.insets = new Insets(0, 0, 5, 0);
		gbc_txtName.anchor = GridBagConstraints.WEST;
		gbc_txtName.gridx = 1;
		gbc_txtName.gridy = 0;
		panelHeadTable.add(txtName, gbc_txtName);
		txtName.setText("Nom");
		txtName.setColumns(10);

		JLabel lblCity = new JLabel("Ville :");
		GridBagConstraints gbc_lblCity = new GridBagConstraints();
		gbc_lblCity.anchor = GridBagConstraints.EAST;
		gbc_lblCity.insets = new Insets(0, 0, 5, 5);
		gbc_lblCity.gridx = 0;
		gbc_lblCity.gridy = 1;
		panelHeadTable.add(lblCity, gbc_lblCity);

		txtCity = new JTextField();
		GridBagConstraints gbc_txtCity = new GridBagConstraints();
		gbc_txtCity.anchor = GridBagConstraints.WEST;
		gbc_txtCity.insets = new Insets(0, 0, 5, 0);
		gbc_txtCity.gridx = 1;
		gbc_txtCity.gridy = 1;
		panelHeadTable.add(txtCity, gbc_txtCity);
		txtCity.setText("Ville");
		txtCity.setColumns(10);

		JLabel lblAddress = new JLabel("Adresse :");
		GridBagConstraints gbc_lblAddress = new GridBagConstraints();
		gbc_lblAddress.anchor = GridBagConstraints.EAST;
		gbc_lblAddress.insets = new Insets(0, 0, 5, 5);
		gbc_lblAddress.gridx = 0;
		gbc_lblAddress.gridy = 2;
		panelHeadTable.add(lblAddress, gbc_lblAddress);

		txtAdresse = new JTextField();
		GridBagConstraints gbc_txtAdresse = new GridBagConstraints();
		gbc_txtAdresse.anchor = GridBagConstraints.WEST;
		gbc_txtAdresse.insets = new Insets(0, 0, 5, 0);
		gbc_txtAdresse.gridx = 1;
		gbc_txtAdresse.gridy = 2;
		panelHeadTable.add(txtAdresse, gbc_txtAdresse);
		txtAdresse.setText("Adresse");
		txtAdresse.setColumns(10);

		JLabel lblManager = new JLabel("Responsable :");
		GridBagConstraints gbc_lblManager = new GridBagConstraints();
		gbc_lblManager.anchor = GridBagConstraints.EAST;
		gbc_lblManager.insets = new Insets(0, 0, 5, 5);
		gbc_lblManager.gridx = 0;
		gbc_lblManager.gridy = 3;
		panelHeadTable.add(lblManager, gbc_lblManager);

		comboBoxManager = new JComboBox();
		GridBagConstraints gbc_comboBoxManager = new GridBagConstraints();
		gbc_comboBoxManager.insets = new Insets(0, 0, 5, 0);
		gbc_comboBoxManager.anchor = GridBagConstraints.WEST;
		gbc_comboBoxManager.gridx = 1;
		gbc_comboBoxManager.gridy = 3;
		panelHeadTable.add(comboBoxManager, gbc_comboBoxManager);

		JPanel panelBtnMenu = new JPanel();
		add(panelBtnMenu, BorderLayout.SOUTH);
		panelBtnMenu.setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));

		JButton btnCancel = new JButton("Annuler");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (store == null)
					mainFrame.displayStoresView();
				else
					mainFrame.displayStoreView(store);
			}
		});
		panelBtnMenu.add(btnCancel);

		btnAdd = new JButton("Ajouter");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (store == null) {
					Store newStore = storeAddEditController.CreateNewStore(txtName.getText(), txtCity.getText(),
							txtAdresse.getText());
					mainFrame.displayStoreView(newStore);
				} else {
					storeAddEditController.EditStore(store, txtName.getText(), txtCity.getText(), txtAdresse.getText());
					storeAddEditController.EditStoreManager(store, (User) comboBoxManager.getSelectedItem());
					mainFrame.displayStoreView(store);
				}
			}
		});
		panelBtnMenu.add(btnAdd);

		JButton btnDelete = new JButton("Supprimer");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int choice = JOptionPane.showConfirmDialog(null,
						"Supprimer se magasin suprimera tout ses rayon, produit, Employer", "Attention",
						JOptionPane.OK_CANCEL_OPTION);
				if (choice == 0) {
					storeAddEditController.deleteStore(store);
					mainFrame.displayStoresView();
				}
			}
		});
		panelBtnMenu.add(btnDelete);

		JPanel panelTitleInformation = new JPanel();
		add(panelTitleInformation, BorderLayout.NORTH);
		panelTitleInformation.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		JLabel lblStore = new JLabel("Store");
		panelTitleInformation.add(lblStore);
	}

	public void refresh(Store store) {
		this.store = store;
		if (store == null) {
			txtAdresse.setText("");
			txtCity.setText("");
			txtName.setText("");
			comboBoxManager.removeAllItems();
			comboBoxManager.setEnabled(false);
			btnAdd.setText("Ajouter");
		} else {
			txtAdresse.setText(store.getAddress());
			txtCity.setText(store.getCity());
			txtName.setText(store.getName());
			comboBoxManager.removeAllItems();
			comboBoxManager.setEnabled(true);
			btnAdd.setText("Modifier");
			ArrayList<User> listManager = storeAddEditController.getPossibleManager(store);
			for (User user : listManager) {
				comboBoxManager.addItem(user);
			}
			comboBoxManager.setSelectedItem(store.getPersonInCharge());
		}
	}

}
