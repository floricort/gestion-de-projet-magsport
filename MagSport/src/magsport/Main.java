package magsport;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import magsport.controller.MagSportController;
import magsport.model.User;
import magsport.view.MainFrame;

public class Main {
	
	/**
	 * Start the application
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			new MainFrame(new MagSportController());
		} catch (SecurityException e) {
			System.out.println("Security error in log creation:");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("I/O error in log creation:");
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			System.out.println("Error, algorithm use to hash password \"" + User.PASSWORD_HASH_METHOD + "\" not found.");
		}
		
	}
}
