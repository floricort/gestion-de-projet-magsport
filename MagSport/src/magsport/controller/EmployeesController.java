package magsport.controller;

import java.util.ArrayList;

import magsport.model.Company;
import magsport.model.Role;
import magsport.model.Store;
import magsport.model.User;

public class EmployeesController {
	MagSportController magSportController;

	/**
	 * Controller
	 * 
	 * @param magSportController
	 */
	public EmployeesController(MagSportController magSportController) {
		this.magSportController = magSportController;
	}

	/**
	 * Return the employees list adapt of power of logged in user All users for ceo
	 * For a store manager: all users of his store
	 * 
	 * @return employees list
	 */
	public ArrayList<User> getEmployeesList() {
		ArrayList<User> emmployeesList = new ArrayList<User>();
		Company company = magSportController.getCompany();
		User loggedInUser = magSportController.getLoggedInUser();
		for (Store store : company.getStores()) {{
			// check permission: if it's CEO or the manager of this store
			if (loggedInUser.getRole() == Role.ROLE_CEO || (loggedInUser.getRole() == Role.ROLE_STORE_MANAGER)
					&& store.getPersonInCharge() == loggedInUser) {
				for (User user : store.getUsers()) 

					emmployeesList.add(user);
				}
			}
		}
		return emmployeesList;
	}
	
	/**
	 * @param user
	 * @return  the affected store of the user
	 */
	public Store getStoreOfEmployee(User user) {
		return magSportController.getCompany().getStoreByPerson(user);
	}
}
