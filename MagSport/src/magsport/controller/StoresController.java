package magsport.controller;

import java.util.ArrayList;

import magsport.model.Store;
import magsport.view.StoresView;



public class StoresController {

	MagSportController magSportController;
	
	
	/**
	 * Controller
	 * @param magSportController
	 */
	public StoresController(MagSportController magSportController) {
		this.magSportController = magSportController;
	}


	public ArrayList<Store> getListStore() {
		return magSportController.getCompany().getStores();
	}


	public Store getStore(String name) {
		ArrayList<Store> list = getListStore();
		for(Store store:list) {
			if (store.getName().equals(name))
				return store;
		}
		return null;
		
	}
	
}
