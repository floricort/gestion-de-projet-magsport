package magsport.controller;

import magsport.model.Department;
import magsport.model.Product;
import magsport.model.User;

public class ProductAddEditController {
	MagSportController magSportController;
	/**
	 * Controller
	 * @param magSportController
	 */
	public ProductAddEditController(MagSportController magSportController) {
		this.magSportController = magSportController;
	}

	public boolean ProductAdd(String name, String code, String description, String stock, Department department){
		User loggeduser = magSportController.getLoggedInUser();
//		for(int i = 0; i < magSportController.getCompany().getStores().size(); i++){
//			for(int j = 0; j < magSportController.getCompany().getStores().get(i).getDepartments().size(); j++){
//				if(loggeduser.equals(magSportController.getCompany().getStores().get(i).getDepartments().get(j).getPersonInCharge())){
					Product product = new Product(Integer.parseInt(code), name, description);
					product.setStock(Integer.parseInt(stock));
					department.addProduct(product);
					magSportController.saveCompany();
					return true;
//			}
//		}
//		}
		//return false;
	}

	public boolean ProductDelete(String code, String stock){
		User loggeduser = magSportController.getLoggedInUser();
		for(int i = 0; i < magSportController.getCompany().getStores().size(); i++){
			for(int j = 0; j < magSportController.getCompany().getStores().get(i).getDepartments().size(); j++){
				if(loggeduser.equals(magSportController.getCompany().getStores().get(i).getDepartments().get(j).getPersonInCharge())){
					for(int k = 0; k < magSportController.getCompany().getStores().get(i).getDepartments().get(j).getProducts().size(); k++){
						Product product = magSportController.getCompany().getStores().get(i).getDepartments().get(j).getProducts().get(k);
						if(product.getIdProduct() == Integer.parseInt(code) && product.getStock() >= Integer.parseInt(stock)){
							int Intstock = product.getStock() - Integer.parseInt(stock);
							magSportController.getCompany().getStores().get(i).getDepartments().get(j).getProducts().get(k).setStock(Intstock);
							magSportController.saveCompany();
							return true;
						}
					}
				}
			}
		}

		return false;
	}
}
