package magsport.controller;

import java.util.ArrayList;

import magsport.model.Role;
import magsport.model.Store;
import magsport.model.User;

public class StoreAddEditController {
	MagSportController magSportController;

	/**
	 * Controller
	 * 
	 * @param magSportController
	 */
	public StoreAddEditController(MagSportController magSportController) {
		this.magSportController = magSportController;
	}

	/**
	 * Create a new store in company
	 * 
	 * @param name
	 * @param city
	 * @param address
	 */
	public Store CreateNewStore(String name, String city, String address) {
		Store store = new Store(name, city, address);
		magSportController.getCompany().addStore(store);
		magSportController.saveCompany();
		return store;
	}

	public void EditStore(Store store, String newName, String NewCity, String NewAddress) {
		store.setAddress(NewAddress);
		store.setCity(NewCity);
		store.setName(newName);
		magSportController.saveCompany();

	}

	public void EditStoreManager(Store store, User newManager) {
		if (store.getUsers().contains(newManager)) {
			store.setPersonInCharge(newManager);
			magSportController.saveCompany();
		}
	}

	public void deleteStore(Store store) {
		if (magSportController.getCompany().getStores().contains(store)) {
			magSportController.getCompany().deleteStore(store);
			magSportController.saveCompany();
		}
	}

	public ArrayList<User> getPossibleManager(Store store) {
		ArrayList<User> listUser = store.getUsers();
		ArrayList<User> listManager = new ArrayList<User>();
		for (User user : listUser) {
			if (user.getRole() == Role.ROLE_STORE_MANAGER || user.getRole() == Role.ROLE_CEO)
				listManager.add(user);
				
	}
		return listManager;}

}
