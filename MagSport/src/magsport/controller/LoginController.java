package magsport.controller;

import java.security.NoSuchAlgorithmException;

import magsport.model.Company;
import magsport.model.User;
import magsport.view.LoginView;
import magsport.view.MainFrame;

public class LoginController {
	
	private MagSportController magSportController;
	
	
	/**
	 * Constructor
	 * @param magSportController
	 */
	public LoginController(MagSportController magSportController) {
		this.magSportController = magSportController;
	}
	
	
	/**
	 * Try a connection with login and password
	 * @param login
	 * @param password
	 */
	public Boolean connection(String login, String password)  {
		Company company = magSportController.getCompany();
		User user = company.getUser(login);
		// if user exist
		if (user != null) {
			// check the password
			try {
				// if it's the good password
				if (user.authentication(password)) {
					// connect user
					magSportController.setLoggedInUser(user);
					return true;
					//TODO
				} else {
					magSportController.getLogger().info("Connection failed");
				}
			} catch (NoSuchAlgorithmException e) {
				magSportController.getLogger().severe("Algorithm to hash password \"" + User.PASSWORD_HASH_METHOD + "\" not found");
				e.printStackTrace();
			}
		}
		// if login don't exist
		else {
			magSportController.getLogger().info("Connection failed");
		}
		return false;
	}
	
}
