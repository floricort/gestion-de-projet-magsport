package magsport.controller;

import java.util.ArrayList;

import magsport.model.Department;
import magsport.model.Role;
import magsport.model.Store;
import magsport.view.StoresView;

public class StoreController {
	MagSportController magSportController;

	/**
	 * Controller
	 * 
	 * @param magSportController
	 */
	public StoreController(MagSportController magSportController) {
		this.magSportController = magSportController;
	}

	public Department getAisel(String name, Store currentStore) {
		ArrayList<Department> listDepartment = currentStore.getDepartments();
		for (Department department : listDepartment)
			if (department.getName().equals(name))
				return department;
		return null;
	}

	public boolean GetUserPermission(Store store) {
		if (magSportController.getLoggedInUser().getRole() == Role.ROLE_CEO) {
			return true;
		}else
			return false;
	}
}
