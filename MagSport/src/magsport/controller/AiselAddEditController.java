package magsport.controller;

import magsport.model.*;

import java.util.ArrayList;

public class AiselAddEditController {
	MagSportController magSportController;
	/**
	 * Controller
	 * @param magSportController
	 */
	public AiselAddEditController(MagSportController magSportController) {
		this.magSportController = magSportController;
	}

	/**
	 * add a department
	 * @param name
	 */
	public void addNewDepartment(String name, Store store){
		Department department = new Department(name);
		store.addDepartment(department);
		//magSportController.saveCompany();
	}

	/**
	 * delete a department
	 * @param department
	 */
	public void deleteDepartment(Department department, Store store){
		ArrayList<Department> departments = store.getDepartments();
		if (departments.contains(department)){
			store.deleteDepartment(department);
		}
		//magSportController.saveCompany();
	}

	public Department findDepartment(String name,Store Store) {
		ArrayList<Department> listDepartment = Store.getDepartments();
		for (Department department : listDepartment)
			if (department.getName().equals(name))
				return department;
		return null;
	}

	public ArrayList<User> getPossibleManager(Store store) {
		ArrayList<User> listUser = store.getUsers();
		ArrayList<User> listManager = new ArrayList<User>();
		for (User user : listUser) {
			if (user.getRole() == Role.ROLE_DEPARTMENT_MANAGER)
				listManager.add(user);
		}
		return listManager;
	}



}
