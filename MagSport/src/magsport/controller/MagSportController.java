package magsport.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import magsport.model.Company;
import magsport.model.Department;
import magsport.model.Store;
import magsport.model.User;
import test.magsport.CompanyExample;



public class MagSportController {

	public final static String COMPANY_FILE = "MagSportCompany.ser";
	public final static String LOG_PATH = "log"; // use "File.separator" if necessary
	public final static String LOG_FILE = "log";

	private Company company;
	private User loggedInUser;
	
	private Logger logger;
	

	
	/**
	 * Controller
	 * @throws IOException 
	 * @throws SecurityException 
	 * @throws NoSuchAlgorithmException 
	 */
	public MagSportController() throws SecurityException, IOException, NoSuchAlgorithmException {
		setupLogger();
		loggedInUser = null;
		
		startApplication();
	}
	
	
	/**
	 * Start the application
	 * @throws NoSuchAlgorithmException 
	 */
	private void startApplication() throws NoSuchAlgorithmException {
		// if save don't exist
		if (!loadCompany()) {
			try {
				company = new Company();
				if(company.getStores().isEmpty()) {
					CompanyExample companyExample = new CompanyExample();
					company = companyExample.getCompanyExample();
				}
				logger.info("New company created");
			} catch (NoSuchAlgorithmException e) {
				logger.severe("Algorithm use to hash password \"" + User.PASSWORD_HASH_METHOD + "\" not found");
				throw e;
			}
		}		
	}
	
	
	/**
	 * Create log directory if not exist
	 */
	private void createLogDirectory() {
		if(!new File(LOG_PATH).exists())
        {
            new File(LOG_PATH).mkdirs();
        }
	}
	
	
	/**
	 * Setup the logger
	 * @throws IOException 
	 * @throws SecurityException 
	 */
	private void setupLogger() throws SecurityException, IOException {
		createLogDirectory();
		logger = Logger.getLogger("MagSportLogger");
		// prepare log file name
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
		String stringDate = simpleDateFormat.format(new Date());
		String fileName = LOG_FILE + "_" + stringDate + ".txt";
		
		// create log file
		FileHandler fileHandler = new FileHandler(LOG_PATH + File.separator + fileName); //file
		SimpleFormatter simple = new SimpleFormatter();
		fileHandler.setFormatter(simple);
		
		// get logger
		logger.addHandler(fileHandler);
	}

	
	/**
	 * Load a company if exist (Serialize the company Object)
	 * @return if a company is loaded
	 */
	private Boolean loadCompany() {
		ObjectInputStream objectInputStream = null;
		try {
			FileInputStream fileInputStream = new FileInputStream(COMPANY_FILE);
			objectInputStream = new ObjectInputStream(fileInputStream);
			// load company
			company = (Company) objectInputStream.readObject();
			// close streams
			objectInputStream.close();
			fileInputStream.close();
			logger.info("Company well loaded");
		}
		catch (IOException exception){
			logger.warning("Error in company loading: Company file \"" + COMPANY_FILE + "\" not found ");
			return false;
		} catch (ClassNotFoundException exception) {
			logger.warning("Error in company loading: Invalid Object in the serialization.");
			return false;
		}
	
		return true;
	}

	
	/**
	 * Save the company in file (serialize)
	 */
	public void saveCompany() {
		ObjectOutputStream objectOutputSteam = null;
		try {
			// open a file stream
			FileOutputStream fileOutputStream = new FileOutputStream(COMPANY_FILE);
			objectOutputSteam = new ObjectOutputStream(fileOutputStream);
			// save the company
			objectOutputSteam.writeObject(company);
			objectOutputSteam.flush();
			objectOutputSteam.close();
			fileOutputStream.close();
		} catch (IOException exception) {
			exception.printStackTrace();
		}
	}
	
	
	/**
	 * Stop the application
	 */
	public void stop() {
		saveCompany();
	}
	
	
	/**
	 * Disconnect the current user
	 */
	public void logout() {
		saveCompany();
		logger.info("user" + loggedInUser.getFirstName() + " well disconected.");
		loggedInUser = null;
	}
	
	
	/**
	 * Get company
	 * @return company
	 */
	public Company getCompany() {
		return company;
	}
	
	
	/**
	 * Set logged in user and open the adapted view
	 * @param user
	 */
	public void setLoggedInUser(User user) {
		loggedInUser = user;
		logger.info("User " + user.getLogin() + " well connected");
	}
	
	
	/**
	 * Get logged in user or null
	 * @return logged in user or null
	 */
	public User getLoggedInUser() {
		return loggedInUser;
	}
	
	
	/**
	 * Return the logger
	 * @return
	 */
	public Logger getLogger() {
		return logger;
	}

	/**
	 * @return the store of the logged user is affected 
	 */
	public Store getStoreOfLoggedUser() {
		return company.getStoreByPerson(loggedInUser);
	}

	/**
	 * @return Department of the logged user is the user in charge of a department 
	 */
	public Department getAiselOfLoggedUser() {
		return company.getAiselByPersonInCharge(loggedInUser);
	}
}
