package magsport.controller;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import magsport.model.Company;
import magsport.model.Department;
import magsport.model.Role;
import magsport.model.Store;
import magsport.model.User;

public class EmployeeController {
	
	MagSportController magSportController;
	
	/**
	 * Controller
	 * @param magSportController
	 */
	public EmployeeController(MagSportController magSportController) {
		this.magSportController = magSportController;
	}
	
	/**
	 * Return responsibilities list
	 * @return responsibilities list
	 */
	public List<Role> getResponsabilities() {
		List<Role> responsabilities = new ArrayList<Role>();
		if (magSportController.getLoggedInUser().getRole()==Role.ROLE_CEO)
			responsabilities.add(Role.ROLE_STORE_MANAGER);
		responsabilities.add(Role.ROLE_DEPARTMENT_MANAGER);
		return responsabilities;
	}
	
	/**
	 * Return stores list adapt of on logged in user power or null
	 * @return stores list
	 */
	public ArrayList<Store> getStores() {
		Company company = magSportController.getCompany();
		User loggedInUser = magSportController.getLoggedInUser();
		ArrayList<Store> stores = new ArrayList<Store>();
		if (loggedInUser != null ) {
			if (loggedInUser.getRole() == Role.ROLE_CEO) {
				stores.addAll(company.getStores());
			}
			else if (loggedInUser.getRole() == Role.ROLE_STORE_MANAGER) {
				stores.add(company.getStoreByPerson(loggedInUser));
			}
		}
		return stores;
	}

	public String createUser(String firstName, String lastName, String login, Store store, Role role) {
		String password = "password";
		User user = null;
		try {
			user = new User(firstName, lastName, login, password, role);
			store.addUser(user);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return password;
		
	}

	/**
	 * Delete the current employee
	 */
	public void deleteEmployee(User employee) {
		if (employee != null) {
			for (Store store : magSportController.getCompany().getStores()) {
				// if it's the store manager
				if (store.getPersonInCharge() != null && store.getPersonInCharge().equals(employee)) {
					store.setPersonInCharge(null);
				}
				for (Department department : store.getDepartments()) {
					// if it's the department manager
					if (department.getPersonInCharge() != null && department.getPersonInCharge().equals(employee)) {
						department.setPersonInCharge(null);
					}
				}
				// remove employee
				store.getUsers().remove(employee);
			}
		}
		
	}

	/**
	 * Reset the user password if exist
	 * @param selectedUser
	 * @return
	 */
	public String resetPassword(User selectedUser) {
		String password = "password";
		if (selectedUser == null) {
			return null;
		}
		try {
			selectedUser.setPassword(password);
		} catch (NoSuchAlgorithmException e) {}
		return password;
	}
}
