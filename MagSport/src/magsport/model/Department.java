package magsport.model;

import java.io.Serializable;
import java.util.ArrayList;



public class Department implements Serializable {
	
    private String name;
    private User personInCharge;
    private ArrayList<Product> products;


    /**
     * Constructor
     * @param name
     */
    public Department(String name){
        this.name = name;
        personInCharge = null;
        products = new ArrayList<Product>();
    }
    
    
    /**
     * Get the department name
     * @return department name
     */
    public String getName() {
        return name;
    }

    
    /**
     * get the person who charge this department or null
     * @return the object user
     */
    public User getPersonInCharge() {
        return personInCharge;
    }

    
    /**
     * get the list of products in this department
     * @return the list of products
     */
    public ArrayList<Product> getProducts() {
        return products;
    }

    
    /**
     * set the name of this department
     * @param name the name of this department
     */
    public void setName(String name) {
        this.name = name;
    }

    
    /**
     * set the person who charges this department
     * @param personInCharge the object user
     */
    public void setPersonInCharge(User personInCharge) {
        this.personInCharge = personInCharge;
    }

    
    /**
     * set the list of products
     * @param products the list of products
     */
    public void setProducts(ArrayList<Product> products) {
        this.products = products;
    }
    
    
    /**
     * Add a product to this department
     * @param product
     */
    public void addProduct(Product product) {
    	products.add(product);
    }

    
    @Override
    public String toString() {
    	return "<" + name + "," + personInCharge + "," + products + ">";
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((personInCharge == null) ? 0 : personInCharge.hashCode());
		result = prime * result + ((products == null) ? 0 : products.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Department other = (Department) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (personInCharge == null) {
			if (other.personInCharge != null)
				return false;
		} else if (!personInCharge.equals(other.personInCharge))
			return false;
		if (products == null) {
			if (other.products != null)
				return false;
		} else if (!products.equals(other.products))
			return false;
		return true;
	}


}
