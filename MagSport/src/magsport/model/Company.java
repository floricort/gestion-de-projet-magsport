package magsport.model;

import test.magsport.CompanyExample;

import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;


public class Company implements Serializable {
	
	private final String CEO_DEFAULT_FIRST_NAME = "CEO first name";
	private final String CEO_DEFAULT_LAST_NAME = "CEO last name";
	private final String CEO_DEFAULT_LOGIN = "ceo";
	private final String CEO_DEFAULT_PASSWORD = "password";

    private User ceo;
    private ArrayList<Store> stores;

    
    /**
     * constructor
     * @throws NoSuchAlgorithmException 
     */
    public Company() throws NoSuchAlgorithmException {
    	ceo = new User(CEO_DEFAULT_FIRST_NAME, CEO_DEFAULT_LAST_NAME, CEO_DEFAULT_LOGIN, CEO_DEFAULT_PASSWORD, Role.ROLE_CEO);
    	stores = new ArrayList<Store>();
    }


    /**
     * get the CEO
     * @return the object CEO
     */
    public User getCeo() {
        return ceo;
    }

    
    /**
     * get the list of stores in this company
     * @return the list of object store
     */
    public ArrayList<Store> getStores() {
        return stores;
    }


    /**
     * set the CEO of company
     * @param ceo the ceo of company
     */
    public void setCeo(User ceo) {
        this.ceo = ceo;
    }

    
    /**
     * set the list of stores in the company
     * @param stores the list of stores
     */
    public void setStores(ArrayList<Store> stores) {
        this.stores = stores;
    }
    
    
    /**
     * Add a store to the company
     * @param store a store
     */
    public void addStore(Store store) {
    	stores.add(store);
    }


    /**
     * Find the user by login
     * @param login the name of login
     * @return the object user
     */
    public User getUser(String login){
    	User user;
    	// if it's the ceo
    	if (login.equals(ceo.getLogin())) {
    		return ceo;
    	}
    	// for all users
    	for (Store store : stores) {
    		user = store.getUser(login);
    		if (user != null) {
    			return user;
    		}
    	}
    	// login not found
    	return null;
    }
    
    /**
     * find the store of person
     * @param personInCharge
     * @return a store or null
     */
    public Store getStoreByPerson(User person) {
    	for (Store store : stores) {
    		if (store.getPersonInCharge() != null && store.getPersonInCharge().equals(person))
    			return store;
    		for (User user : store.getUsers())
    			if (user.equals(person))
    				return store;
    	}
    	return null;
    }
    
    /**
     *  find the store of person
     * @param person
     * @return
     */
	public Department getAiselByPersonInCharge(User person) {
		for (Store store : stores) {
    		for (Department department : store.getDepartments())
    			if (department.getPersonInCharge() != null && department.getPersonInCharge().equals(person))
    				return department;
    	}
		return null;
	}
	
    /**
     * Convert Company to string
     */
    public String toString() {
    	String result = "<Company:\n";
    	result = result + "\t" + ceo.toString() + "\n";
    	result = result + "\t" + stores.toString() + "\n";
    	result = result + ">\n";
    	return result;
    }


	@Override
	public int hashCode() {
		return ceo.hashCode() + stores.hashCode();
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Company other = (Company) obj;
		if (ceo == null) {
			if (other.ceo != null)
				return false;
		} else if (!ceo.equals(other.ceo))
			return false;
		if (stores == null) {
			if (other.stores != null)
				return false;
		} else if (!stores.equals(other.stores))
			return false;
		return true;
	}


	public void deleteStore(Store store) {
		stores.remove(store);
	}



}
