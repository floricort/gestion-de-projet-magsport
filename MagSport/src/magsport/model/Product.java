package magsport.model;

import java.io.Serializable;



public class Product implements Serializable {
	
    private Integer idProduct;
    private String name;
    private String description;
    private Integer stock;

    
    /**
     * constructor
     * @param idProduct the id of product
     * @param name the name of product
     *
     */
    public Product(Integer idProduct, String name,String description){
        this.idProduct = idProduct;
        this.name = name;
        this.description = description;
        this.stock = 0;
    }

    
    /**
     * get the id of product
     * @return the id of product
     */
    public Integer getIdProduct() {
        return idProduct;
    }

    
    /**
     * get the name of product
     * @return the name of product
     */
    public String getName() {
        return name;
    }

    
    /**
     * get the description of product
     * @return the description of product
     */
    public String getDescription() {
        return description;
    }

    
    /**
     * get the number of stock of this product
     * @return the number of stock
     */
    public Integer getStock() {
        return stock;
    }

    
    /**
     * set the id of product
     * @param idProduct the id of product
     */
    public void setIdProduct(Integer idProduct) {
        this.idProduct = idProduct;
    }

    
    /**
     * set the name of product
     * @param name the name of product
     */
    public void setName(String name) {
        this.name = name;
    }

    
    /**
     * set the description of product
     * @param description the description of product
     */
    public void setDescription(String description) {
        this.description = description;
    }

    
    /**
     * set the number of stock for this product
     * @param stock the number of stock
     */
    public void setStock(Integer stock) {
        this.stock = stock;
    }
    
    
    @Override
    public String toString() {
    	return "<" + idProduct + "," + name + "," + description + "," + stock + ">";
    }


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((idProduct == null) ? 0 : idProduct.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((stock == null) ? 0 : stock.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (idProduct == null) {
			if (other.idProduct != null)
				return false;
		} else if (!idProduct.equals(other.idProduct))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (stock == null) {
			if (other.stock != null)
				return false;
		} else if (!stock.equals(other.stock))
			return false;
		return true;
	}
    
    
    
}
