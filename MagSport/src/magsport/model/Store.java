package magsport.model;

import java.io.Serializable;
import java.util.ArrayList;


public class Store implements Serializable {
	
    private String name;
    private String city;
    private String address;
    private User personInCharge;
    private ArrayList<Department> departments;
    private ArrayList<User> users;


    /**
     * constructor
     * @param name the name of store
     */
    public Store(String name,String city, String address){
        this.name = name;
        this.city = city;
        this.address = address;
        personInCharge = null;
        departments = new ArrayList<Department>();
        users = new ArrayList<User>();
    }
    
    
    /**
     * get the name of store
     * @return the name of store
     */
    public String getName() {
        return name;
    }

    
    /**
     * get the city of the store
     * @return the city of the store
     */
    public String getCity() {
        return city;
    }

    
    /**
     * get the address of the store
     * @return the address of the store
     */
    public String getAddress() {
        return address;
    }
    

    /**
     * get the person who charges this store or null
     * @return the object user
     */
    public User getPersonInCharge() {
        return personInCharge;
    }

    
    /**
     * get the list of departments of this store
     * @return the list of departments
     */
    public ArrayList<Department> getDepartments() {
        return departments;
    }

    /**
     * get the list of users of this store
     * @return the list of users
     */
    public ArrayList<User> getUsers() {
        return users;
    }

    /**
     * set the name of this store
     * @param name the name of store
     */
    public void setName(String name) {
        this.name = name;
    }

    
    /**
     * set the city of this store
     * @param city the city
     */
    public void setCity(String city) {
        this.city = city;
    }

    
    /**
     * set the address of this store
     * @param address the address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    
    /**
     * set the person who charges this store
     * @param personInCharge the object user
     */
    public void setPersonInCharge(User personInCharge) {
        this.personInCharge = personInCharge;
    }

    
    /**
     * set the list of departments in this store
     * @param departments  the list of departments
     */
    public void setDepartments(ArrayList<Department> departments) {
        this.departments = departments;
    }
    
    
    /**
     * Add a department to this store
     * @param department
     */
    public void addDepartment(Department department) {
    	departments.add(department);
    }

    /**
     * delete a department of this store
     * @param department
     */
    public void deleteDepartment(Department department){ departments.remove(department); }
    
    /**
     * set the list of users in this store
     * @param users the list of user
     */
    public void setUsers(ArrayList<User> users) {
        this.users = users;
    }
    
    
    /**
     * Add user to this store
     * @param user
     */
    public void addUser(User user) {
    	users.add(user);
    }

    /**
     * Find and return the user by login or null if not found
     * @param login the name of login
     * @return the object user or null
     */
    public User getUser(String login){
    	for (User user : users) {
    		if (user.getLogin().equals(login)) {
    			return user;
    		}
    	}
        return null;
    }


    
    @Override
    public String toString() {
    	String result = "<" + name + "," + city + "," + address + ",";
    	result = result + personInCharge + ">";
    	return result;
    }


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result + ((departments == null) ? 0 : departments.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((personInCharge == null) ? 0 : personInCharge.hashCode());
		result = prime * result + ((users == null) ? 0 : users.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Store other = (Store) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (departments == null) {
			if (other.departments != null)
				return false;
		} else if (!departments.equals(other.departments))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (personInCharge == null) {
			if (other.personInCharge != null)
				return false;
		} else if (!personInCharge.equals(other.personInCharge))
			return false;
		if (users == null) {
			if (other.users != null)
				return false;
		} else if (!users.equals(other.users))
			return false;
		return true;
	}
    
    
    
}
