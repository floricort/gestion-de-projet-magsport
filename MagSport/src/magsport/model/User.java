package magsport.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;



public class User implements Serializable {
	
	public final static String PASSWORD_HASH_METHOD = "SHA-256";

    private String firstName;
    private String lastName;
    private String login;
    private String password;
    private Role role;
	

    /**
     * constructor
     * @param firstName the first name of user
     * @param lastName the last name of user
     * @param login the name of login
     * @param password the password
     */
    public User(String firstName,String lastName, String login, String password, Role role) throws NoSuchAlgorithmException {
        this.firstName = firstName;
        this.lastName = lastName;
        this.login = login;
        this.password = hash(password);
        this.role = role;
    }
    
    
    /**
     * get the first name of user
     * @return the first name of user
     */
    public String getFirstName() {
        return firstName;
    }

    
    /**
     * get the last name of user
     * @return the last name of user
     */
    public String getLastName() {
        return lastName;
    }

    
    /**
     * get the name of login
     * @return the name of login
     */
    public String getLogin() {
        return login;
    }

    
    /**
     * get the password
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    
    /**
     * get the role of user
     * @return the role
     */
    public Role getRole() {
        return role;
    }

    
    /**
     * set the first name of user
     * @param firstName the first name of user
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    
    /**
     * set the last name of user
     * @param lastName the last name of user
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    

    /**
     * set the name of login
     * @param login the name of login
     */
    public void setLogin(String login) {
        this.login = login;
    }
    

    /**
     * set the password
     * @param password the password
     */
    public void setPassword(String password) throws NoSuchAlgorithmException {
        this.password = hash(password);
    }
    

    /**
     * set the role of user
     * @param role the role
     */
    public void setRole(Role role) {
        this.role = role;
    }

    
    /**
     * Hash the password by MD5
     * @param password the password
     * @return the string encrypted
     * @throws NoSuchAlgorithmException
     */
    public String hash(String password) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance(PASSWORD_HASH_METHOD);
        md.update(password.getBytes());
        String str = new BigInteger(1,md.digest()).toString(16);
        return str;
    }

    
    /**
     * The function of login this user
     * @param password the password
     * @return true: login successfully false: failed
     * @throws NoSuchAlgorithmException the used algorithm not found
     */
    public Boolean authentication (String password) throws NoSuchAlgorithmException {
        password = hash(password);
        return this.password.equals(password);
    }
    
 
    /**
     * Return a string who represent user
     */
    @Override
    public String toString() {
    	String result = "<" + firstName + " " + lastName + "," + role + ">";
    	return result;
    }


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((login == null) ? 0 : login.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((role == null) ? 0 : role.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (login == null) {
			if (other.login != null)
				return false;
		} else if (!login.equals(other.login))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (role != other.role)
			return false;
		return true;
	}
    
    
    
}
